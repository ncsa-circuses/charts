# oauth2-proxy-traefik

## Overview

This chart deploys [`oauth2-proxy-traefik`](https://oauth2-proxy.github.io/oauth2-proxy/) to secure arbitrary URL endpoints with an OIDC provider such as Keycloak.

## Keycloak configuration

The Keycloak client for `oauth2-proxy-traefik` must be configured with two mappers:

* Audience: Select the `oauth2-proxy-traefik` client from the "Intended Client Audience" list
* Group Membership: Set token claim name to "groups"
