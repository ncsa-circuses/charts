#!/bin/bash

set -e

declare -a backup_names=()
getArray() {
    while IFS= read -r line # Read a line
    do
        backup_names+=("$line") # Append line to the array
    done < "$1"
}

markdown_log() {
    message=$1
    type=$2
    if [[ "$type" == "h1" ]]; then
        echo -e "# $message\n"
    fi
    if [[ "$type" == "h2" ]]; then
        echo -e "## $message\n"
    fi
    if [[ "$type" == "h3" ]]; then
        echo -e "### $message\n"
    fi
    if [[ "$type" == "h4" ]]; then
        echo -e "#### $message\n"
    fi
    if [[ "$type" == "codeblock" ]]; then
        echo -e '```bash'
        echo -e "$message"
        echo -e '```\n'
    fi
}

# CONF_FILE=""
BACKUP_NAME_FILE=""
ARG_UNLOCK=""
PARAMS=""
while (( "$#" )); do
  case "$1" in
    -b|--backup-list)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        BACKUP_NAME_FILE=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        $0 -h
        exit 1
      fi
      ;;
    # -c|--config)
    #   if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
    #     CONF_FILE=$2
    #     shift 2
    #     if [[ ! -f "${CONF_FILE}" ]]; then
    #       echo "Specified config file \"${CONF_FILE}\" not found. Aborting." >&2
    #       exit 1
    #     fi
    #   else
    #     echo "Error: Argument for $1 is missing" >&2
    #     $0 -h
    #     exit 1
    #   fi
    #   ;;
    -u|--unlock)
      ARG_UNLOCK="unlock"
      shift
      ;;
    -h|--help)
      __usage="
Usage:  $(basename $0) [OPTIONS]

Generate a Markdown-formatted summary of recent DecentCI backups using Restic.

  -u, --unlock               Attempt to remove stale restic repo locks
  -b, --backup-list string   Path to backup name list file

Export an environment variable RESTIC_REPOSITORY_BASEURL of the form below. It will be appended
with the backup name to form the complete RESTIC_REPOSITORY value required by Restic.

    export RESTIC_REPOSITORY_BASEURL="rest:https://username:password@restic.example.com/username"

Restic also needs the repo password, so export RESTIC_PASSWORD as well:

    export RESTIC_PASSWORD="3c76874010dfff6feed4894de0ee134c084a07c8"

Construct a file \"backups.csv\" with a list of backup names, one per line:

    backup-myapp-files
    backup-myapp-db
    ...

Given these files, an example command is:

    $(basename $0) -b backups.csv | tee backup-summary.md
"
      echo "$__usage"
      exit 0
      ;;
    -*|--*=) # unsupported flags
      echo "Error: Unsupported flag $1" >&2
      $0 -h
      exit 1
      ;;
    *) # preserve positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

if [[ ! -f "${BACKUP_NAME_FILE}" ]]; then
  echo "Specified backup list file \"${BACKUP_NAME_FILE}\" not found. Aborting." >&2
  $0 -h
  exit 1
fi

getArray "${BACKUP_NAME_FILE}"

markdown_log "Summary of Restic backups" "h1"
echo "- Backup list file : \`${BACKUP_NAME_FILE}\`"
if [[ "${ARG_UNLOCK}" == "unlock" ]]; then
  echo "- Will attempt to unlock repos."
fi
echo

for backup_name in "${backup_names[@]}"
do
    markdown_log "Backup \`$backup_name\`" "h2"
    ## Populate env vars required by Restic to access the backup repo
    if [[ -z $RESTIC_REPOSITORY_BASEURL || -z $RESTIC_PASSWORD ]]; then
      echo 'Error: RESTIC_REPOSITORY_BASEURL and RESTIC_PASSWORD must be set.' >&2
      exit 1
    fi
    export RESTIC_REPOSITORY="${RESTIC_REPOSITORY_BASEURL}/${backup_name}/"
    ## If the unlock option was invoked, attempt to remove the stale repo lock.
    if [[ "$ARG_UNLOCK" == "unlock" ]]; then
        echo -e "\nUnlocking restic repo for backup \`$backup_name\`...\n"
        UNLOCK_OUTPUT=$(restic unlock)
        if [[ "$UNLOCK_OUTPUT" != "" ]]; then
          echo -e '```'
          echo -e "$UNLOCK_OUTPUT"
          echo -e '```\n'
        fi
    fi
    markdown_log "Statistics" "h3"
    echo -e "\nListing restic stats for backup \`$backup_name\`:\n"
    echo -e '```'
    restic stats
    echo -e '```\n'
    markdown_log "Snapshots" "h3"
    echo -e "\nListing 5 most recent restic snapshots for backup \`$backup_name\`:\n"
    echo -e '```'
    restic snapshots --latest 5
    echo -e '```\n'
    markdown_log "Files" "h3"
    echo -e "\nListing files in latest snapshot of \`$backup_name\`:\n"
    echo -e '```'
    restic ls latest | head -n 20
    echo -e "...\n"
    echo -e '```\n'
    ## If there are at least two snapshots to compare, output the differences.
    if [[ $(restic snapshots --json | jq -r '. | length') > 1 ]]; then
        markdown_log "Differences" "h3"
        echo -e "\nListing differences between latest two snapshots of \`$backup_name\`:\n"
        echo -e '```'
        restic diff $(restic snapshots --json | jq -r '.[-2:][].id') 
        echo -e '```\n'
    fi
done

echo "Backup summary complete"
exit 0
