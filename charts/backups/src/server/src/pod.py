from kubernetes import client
from kubernetes.client.rest import ApiException
import json
import os
from globals import log, annotation_namespace, get_backup_agent_name

def get_pod_by_name(name, namespace='default'):
    pod_list = get_agent_pod_list(namespace)
    for pv in pod_list:
        if name == pv['metadata']['name']:
            return pv
    return None


def get_agent_pod_list(namespace='default'):
    v1 = client.CoreV1Api()
    ret = v1.list_namespaced_pod(namespace=namespace, _preload_content=False)
    pod_list = json.loads(ret.data)
    # log.debug(pod_list)
    return [item for item in pod_list["items"]
            if 'labels' in item['metadata']
            and 'app.kubernetes.io/managed-by' in item['metadata']['labels']
            and item['metadata']['labels']['app.kubernetes.io/managed-by'] == annotation_namespace]


def create_namespaced_pod(backup_config={}, namespace='default', owner=None):
    pod_body = render_pod_body(backup_config=backup_config, namespace=namespace, owner=owner)
    assert pod_body
    name = pod_body['metadata']['name']
    if get_pod_by_name(name=name, namespace=namespace):
        log.warning(f"Pod {name} already exists. Nothing to create.")
    else:
        v1 = client.CoreV1Api()
        ret = v1.create_namespaced_pod(namespace=namespace, body=pod_body, pretty=True,
                                                           _preload_content=False, async_req=False)
        ret_dict = json.loads(ret.data)
        log.debug(f'''Pod "{name}" created.''')


def delete_namespaced_pod(namespace='default', name=None):
    assert name
    if not get_pod_by_name(name=name, namespace=namespace):
        log.warning(f"Pod {name} not found. Nothing to delete.")
    else:
        v1 = client.CoreV1Api()
        ret = v1.delete_namespaced_pod(
            name=name, namespace=namespace, _preload_content=False, async_req=False)
        ret_dict = json.loads(ret.data)
        log.debug(f'''Deleting pod "{name}" ...''')


def render_pod_body(backup_config={}, namespace='default', owner=None):
    backup_name = backup_config['backup_name']
    agent_name = get_backup_agent_name(backup_name)
    if backup_config['engine'] == 'restic':
        volumes = []
        volumeMounts = []
    elif backup_config['engine'] == 'duplicati':
        ## If an unmanaged PVC was specified, use that; otherwise, assume the managed PVC
        ## named the same as the cronjob
        backup_pvc_name = backup_config['pvc_name_backup'] if backup_config['pvc_name_backup'] else backup_name
        volumes = [
            {
                'name': 'data-backup',
                'persistentVolumeClaim': {
                    'claimName': backup_pvc_name,
                }
            },
        ]
        volumeMounts = [
            {
                'name': 'data-backup',
                'subPath': 'duplicati_db',
                'mountPath': '/opt/duplicati'
            },
            {
                'name': 'data-backup',
                'subPath': backup_name,
                'mountPath': '/backup',
            },
        ]
    env = [
        {
            'name': 'BACKUP_ENGINE',
            'value': backup_config['engine'],
        },
        {
            'name': 'RETENTION_POLICY',
            'value': backup_config['retention_policy'],
        },
        {
            'name': 'BACKUP_NAME',
            'value': backup_name,
        },
    ]
    ## If the backup is a direct file-based backup, mount the source data
    ## NOT READ-ONLY to allow for restoring data from backups
    if backup_config['pvc_name_src']:
        volumes.append({
            'name': 'data-source',
            'persistentVolumeClaim': {
                'claimName': backup_config['pvc_name_src'],
            },
        })
        volumeMounts.append({
            'name': 'data-source',
            'mountPath': '/data',
            'readOnly': False,
        })
    if backup_config['restic']['repo']['configFileKey']:
        volumes.append({
            'name': 'repo-config-file',
            'secret': {
                'secretName': backup_config['restic']['repo']['existingSecret'],
            },
        })
        volumeMounts.append({
            'name': 'repo-config-file',
            'mountPath': '/etc/restic.conf',
            'subPath': backup_config['restic']['repo']['configFileKey'],
            'readOnly': True,
        })
    ## If the backup is a database backup, provide access details
    if backup_config['database']:
        env.extend([
            {
                'name': 'DB_TYPE',
                'value': backup_config['database']['type'],
            },
            {
                'name': 'DB_HOST',
                'value': backup_config['database']['host'],
            },
            {
                'name': 'DB_NAME',
                'value': backup_config['database']['auth']['database'],
            },
            {
                'name': 'DB_USER',
                'value': backup_config['database']['auth']['username'],
            },
            {
                'name': 'DB_PASS',
                'valueFrom': {
                    'secretKeyRef': {
                        'name': backup_config['database']['auth']['existingSecret'],
                        'key': backup_config['database']['auth']['passwordKey'],
                    }
                }
            },
        ])
    if backup_config['engine'] == 'restic':
        env.extend([
            {
                'name': 'RESTIC_PASSWORD',
                'valueFrom': {
                    'secretKeyRef': {
                        'name': backup_config['restic']['repo']['existingSecret'],
                        'key': backup_config['restic']['repo']['passwordKey'],
                    }
                }
            },
            {
                'name': 'RESTIC_FORGET_ARGS',
                'value': backup_config['restic']['repo']['forgetArgs'],
            },
        ])
        ## If a base URL for RESTIC_REPOSITORY has already been provided, append the backup name.
        if backup_config['restic']['repo']['resticRepositoryBase']:
            env.extend([
                {
                    'name': 'RESTIC_REPOSITORY',
                    'value': f'''{backup_config['restic']['repo']['resticRepositoryBase']}/{backup_name}''',
                },
            ])
        ## If a value for RESTIC_REPOSITORY has not been provided, assume the default case
        ## where the backup target is a Restic Server.
        else:
            env.extend([
                {
                    'name': 'RESTIC_SERVER_BASEURL',
                    'value': backup_config['restic']['server']['baseurl'],
                },
                {
                    'name': 'RESTIC_SERVER_PROTOCOL',
                    'value': backup_config['restic']['server']['protocol'],
                },
                {
                    'name': 'RESTIC_SERVER_USER',
                    'valueFrom': {
                        'secretKeyRef': {
                            'name': backup_config['restic']['server']['existingSecret'],
                            'key': backup_config['restic']['server']['usernameKey'],
                        }
                    }
                },
                {
                    'name': 'RESTIC_SERVER_PASS',
                    'valueFrom': {
                        'secretKeyRef': {
                            'name': backup_config['restic']['server']['existingSecret'],
                            'key': backup_config['restic']['server']['passwordKey'],
                        }
                    }
                },
                {
                    'name': 'RESTIC_REPOSITORY',
                    'value': "rest:$(RESTIC_SERVER_PROTOCOL)://$(RESTIC_SERVER_USER):$(RESTIC_SERVER_PASS)@$(RESTIC_SERVER_BASEURL)/$(RESTIC_SERVER_USER)/$(BACKUP_NAME)/",
                },
            ])

    image = os.environ['AGENT_IMAGE']
    if image.split(':')[1] in ['latest', 'dev']:
        imagePullPolicy = "Always"
    else:
        imagePullPolicy = "IfNotPresent"
    ## The purpose of this agent is for manual interaction via `kubectl exec`
    ## so the command is to sleep forever
    args = ['-c', 'sleep 1000d']
    ## TODO: Should this be a Deployment instead? That way the pod can respawn as it 
    ##       would be expected to do in the event of a node drain, etc.
    body = {
        'apiVersion': 'v1',
        'kind': 'Pod',
        'metadata': {
            'name': agent_name,
            'namespace': namespace,
            'annotations': {
                f'{annotation_namespace}/config': json.dumps(backup_config),
                f'{annotation_namespace}/owner-apiVersion': f'''{owner.api_version}''',
                f'{annotation_namespace}/owner-kind': f'''{owner.kind}''',
                f'{annotation_namespace}/owner-name': f'''{owner.metadata.name}''',
            },
            'labels': {
                'app.kubernetes.io/managed-by': annotation_namespace,
            },
        },
        'spec': {
            'volumes': volumes,
            'containers': [
                {
                    'name': 'backup-agent',
                    'image': image,
                    'imagePullPolicy': imagePullPolicy,
                    'command': ['/bin/bash'],
                    'args': args,
                    'env': env,
                    'volumeMounts': volumeMounts,
                },
            ],
            'restartPolicy': 'Always'
        },
    }

    return body

def patch_agent(agent_pod, backup_config={}, owner=None):
    namespace = agent_pod['metadata']['namespace']
    v1 = client.CoreV1Api()

    try:
        v1.patch_namespaced_pod(
            agent_pod['metadata']['name'], 
            namespace=namespace,
            body=render_pod_body(
                backup_config=backup_config,
                namespace=namespace,
                owner=owner
            )
        )
    except ApiException as e:
        print("Exception when calling CoreV1Api->patch_namespaced_pod: %s\n" % e)
        raise
