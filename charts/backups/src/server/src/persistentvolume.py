from kubernetes import client
from kubernetes.client.rest import ApiException
import json
from globals import log, annotation_namespace


# def pv_exists(name):
#     v1 = client.CoreV1Api()
#     pv = v1.read_persistent_volume(name)
#     log.debug(pv)
#     if pv:
#         return True
#     else:
#         return False


def get_pv_by_name(name):
    pv_list = get_backup_pv_list()
    for pv in pv_list:
        if name == pv['metadata']['name']:
            return pv
    return None


def get_backup_pv_list():
    v1 = client.CoreV1Api()
    ret = v1.list_persistent_volume(_preload_content=False)
    pv_list = json.loads(ret.data)
    # log.debug(pv_list)
    return [item for item in pv_list["items"]
            if 'labels' in item['metadata']
            and 'app.kubernetes.io/managed-by' in item['metadata']['labels']
            and item['metadata']['labels']['app.kubernetes.io/managed-by'] == annotation_namespace]


def create_pv(backup_config={}, owner=None):
    pv_body = render_pv_body(backup_config=backup_config, owner=owner)
    assert pv_body
    name = pv_body['metadata']['name']
    if get_pv_by_name(name):
        log.warning(f"PV {name} already exists. Nothing to create.")
    else:
        v1 = client.CoreV1Api()
        ret = v1.create_persistent_volume(body=pv_body, pretty=True,
                                          _preload_content=False, async_req=False)
        ret_dict = json.loads(ret.data)
        log.debug(f'''"{name}" PV created.''')


def delete_pv(name=None):
    assert name
    if not get_pv_by_name(name):
        log.warning(f"PV {name} not found. Nothing to delete.")
    else:
        v1 = client.CoreV1Api()
        ret = v1.delete_persistent_volume(
            name=name, _preload_content=False, async_req=False)
        ret_dict = json.loads(ret.data)
        log.debug(f'''Deleting PV "{name}" ...''')


def render_pv_body(backup_config={}, owner=None):
    backup_name = backup_config['backup_name']
    body = {
        'apiVersion': 'v1',
        'kind': 'PersistentVolume',
        'metadata': {
            'name': backup_name,
            'annotations': {
                f'{annotation_namespace}/config': json.dumps(backup_config),
                f'{annotation_namespace}/owner-apiVersion': f'''{owner.api_version}''',
                f'{annotation_namespace}/owner-kind': f'''{owner.kind}''',
                f'{annotation_namespace}/owner-name': f'''{owner.metadata.name}''',
            },
            'labels': {
                'app.kubernetes.io/managed-by': annotation_namespace,
                'name': backup_name,
            },
        },
        'spec': {
            'accessModes': [
                'ReadWriteMany'
            ],
            'capacity': {
                'storage': '1Gi'
            },
            'nfs': {
                'path': backup_config['nfs_path'],
                'server': backup_config['nfs_server'],
            },
            'persistentVolumeReclaimPolicy': 'Retain',
        },
    }
    return body

def patch_pv(pv, backup_config={}, owner=None):
    log.debug(f'''Updating PV for "{backup_config['backup_name']}".''')
    v1 = client.CoreV1Api()
    try:
        v1.patch_persistent_volume(pv['metadata']['name'],
            body=render_pv_body(backup_config=backup_config, owner=owner))
    except ApiException as e:
        print("Exception when calling CoreV1Api->patch_persistent_volume: %s\n" % e)
        raise
