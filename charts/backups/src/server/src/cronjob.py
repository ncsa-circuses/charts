from kubernetes import client
from kubernetes.client.rest import ApiException
import json
import yaml
import os
from globals import log, annotation_namespace
from jinja2 import Template

def cronjob_exists(namespace, name):
    '''ref: https://github.com/kubernetes-client/python/blob/master/examples/cronjob_crud.py'''
    cron_job_list = get_backup_cronjob_list(namespace)
    for cron_job in cron_job_list:
        if name == cron_job['metadata']['name']:
            return True
    return False

def get_cronjob_by_name(name, namespace='default'):
    cron_job_list = get_backup_cronjob_list(namespace)
    for cron_job in cron_job_list:
        if name == cron_job['metadata']['name']:
            return cron_job
    return None


def get_backup_cronjob_list(namespace='default'):
    '''ref: https://github.com/kubernetes-client/python/blob/master/examples/cronjob_crud.py'''
    v1 = client.BatchV1Api()
    ret = v1.list_namespaced_cron_job(
        namespace=namespace, _preload_content=False)
    cron_job_list = json.loads(ret.data)
    return [item for item in cron_job_list["items"]
            if 'labels' in item['metadata']
            and 'app.kubernetes.io/managed-by' in item['metadata']['labels']
            and item['metadata']['labels']['app.kubernetes.io/managed-by'] == annotation_namespace]


def create_namespaced_cron_job(backup_config, namespace='default', owner=None):
    '''ref: https://github.com/kubernetes-client/python/blob/master/examples/cronjob_crud.py'''
    body = render_cronjob_body(backup_config=backup_config, namespace=namespace, owner=owner)
    name = body['metadata']['name']
    if cronjob_exists(namespace, name):
        log.warning(f"CronJob {name} already exists. Nothing to create.")
    else:
        v1 = client.BatchV1Api()
        ret = v1.create_namespaced_cron_job(namespace=namespace, body=body, pretty=True,
                                            _preload_content=False, async_req=False)
        ret_dict = json.loads(ret.data)
        log.debug(f'''"{name}" created.''')


def delete_namespaced_cron_job(namespace='default', name=None):
    '''ref: https://github.com/kubernetes-client/python/blob/master/examples/cronjob_crud.py'''
    assert name
    if not cronjob_exists(namespace, name):
        log.warning(f"CronJob {name} not found. Nothing to delete.")
    else:
        v1 = client.BatchV1Api()
        ret = v1.delete_namespaced_cron_job(name=name, namespace=namespace, _preload_content=False, async_req=False)
        ret_dict = json.loads(ret.data)
        log.debug(f'''Deleting CronJob "{name}" ...''')

def render_cronjob_body(backup_config={}, namespace='default', owner=None):
    backup_name = backup_config['backup_name']
    if backup_config['engine'] == 'restic':
        volumes = []
        volumeMounts = []
    elif backup_config['engine'] == 'duplicati':
        ## If an unmanaged PVC was specified, use that; otherwise, assume the managed PVC
        ## named the same as the cronjob
        backup_pvc_name = backup_config['pvc_name_backup'] if backup_config['pvc_name_backup'] else backup_name
        volumes = [
            {
                'name': 'data-backup',
                'persistentVolumeClaim': {
                    'claimName': backup_pvc_name,
                }
            },
        ]
        volumeMounts = [
            {
                'name': 'data-backup',
                'subPath': 'duplicati_db',
                'mountPath': '/opt/duplicati'
            },
            {
                'name': 'data-backup',
                'subPath': backup_name,
                'mountPath': '/backup',
            },
        ]
    env = [
        {
            'name': 'BACKUP_ENGINE',
            'value': backup_config['engine'],
        },
        {
            'name': 'RETENTION_POLICY',
            'value': backup_config['retention_policy'],
        },
        {
            'name': 'BACKUP_NAME',
            'value': backup_name,
        },
    ]
    alert_message = f'''Backup failure: {backup_name}'''

    ## If the backup is a direct file-based backup, mount the source data read-only
    if backup_config['pvc_name_src']:
        args = [f'''bash run_backup.sh files''']
        volumes.append({
            'name': 'data-source',
            'persistentVolumeClaim': {
                'claimName': backup_config['pvc_name_src'],
            },
        })
        volumeMounts.append({
            'name': 'data-source',
            'mountPath': '/data',
            'readOnly': True,
        })
    if backup_config['restic']['repo']['configFileKey']:
        volumes.append({
            'name': 'repo-config-file',
            'secret': {
                'secretName': backup_config['restic']['repo']['existingSecret'],
            },
        })
        volumeMounts.append({
            'name': 'repo-config-file',
            'mountPath': '/etc/restic.conf',
            'subPath': backup_config['restic']['repo']['configFileKey'],
            'readOnly': True,
        })
    ## If the backup is a database backup, provide access details
    if backup_config['database']:
        args = [f'''bash run_backup.sh db''']
        env.extend([
            {
                'name': 'DB_TYPE',
                'value': backup_config['database']['type'],
            },
            {
                'name': 'DB_HOST',
                'value': backup_config['database']['host'],
            },
            {
                'name': 'DB_NAME',
                'value': backup_config['database']['auth']['database'],
            },
            {
                'name': 'DB_USER',
                'value': backup_config['database']['auth']['username'],
            },
            {
                'name': 'DB_PASS',
                'valueFrom': {
                    'secretKeyRef': {
                        'name': backup_config['database']['auth']['existingSecret'],
                        'key': backup_config['database']['auth']['passwordKey'],
                    }
                }
            },
        ])
    if backup_config['engine'] == 'restic':
        env.extend([
            {
                'name': 'RESTIC_PASSWORD',
                'valueFrom': {
                    'secretKeyRef': {
                        'name': backup_config['restic']['repo']['existingSecret'],
                        'key': backup_config['restic']['repo']['passwordKey'],
                    }
                }
            },
            {
                'name': 'RESTIC_FORGET_ARGS',
                'value': backup_config['restic']['repo']['forgetArgs'],
            },
        ])
        ## If a base URL for RESTIC_REPOSITORY has already been provided, append the backup name.
        if backup_config['restic']['repo']['resticRepositoryBase']:
            env.extend([
                {
                    'name': 'RESTIC_REPOSITORY',
                    'value': f'''{backup_config['restic']['repo']['resticRepositoryBase']}/{backup_name}''',
                },
            ])
        ## If a value for RESTIC_REPOSITORY has not been provided, assume the default case
        ## where the backup target is a Restic Server.
        else:
            env.extend([
                {
                    'name': 'RESTIC_SERVER_BASEURL',
                    'value': backup_config['restic']['server']['baseurl'],
                },
                {
                    'name': 'RESTIC_SERVER_PROTOCOL',
                    'value': backup_config['restic']['server']['protocol'],
                },
                {
                    'name': 'RESTIC_SERVER_USER',
                    'valueFrom': {
                        'secretKeyRef': {
                            'name': backup_config['restic']['server']['existingSecret'],
                            'key': backup_config['restic']['server']['usernameKey'],
                        }
                    }
                },
                {
                    'name': 'RESTIC_SERVER_PASS',
                    'valueFrom': {
                        'secretKeyRef': {
                            'name': backup_config['restic']['server']['existingSecret'],
                            'key': backup_config['restic']['server']['passwordKey'],
                        }
                    }
                },
                {
                    'name': 'RESTIC_REPOSITORY',
                    'value': "rest:$(RESTIC_SERVER_PROTOCOL)://$(RESTIC_SERVER_USER):$(RESTIC_SERVER_PASS)@$(RESTIC_SERVER_BASEURL)/$(RESTIC_SERVER_USER)/$(BACKUP_NAME)/",
                },
            ])
    ## If alerts are configured, add the env vars
    try:
        env.extend([
            {
                'name': 'MATRIX_ALERT_URL',
                'valueFrom': {
                    'secretKeyRef': {
                        'name': backup_config['alerts']['matrix']['existingSecret'],
                        'key': backup_config['alerts']['matrix']['urlKey'],
                    }
                }
            },
        ])
    except:
        pass
    image = os.environ['CRONJOB_IMAGE']
    if image.split(':')[1] in ['latest', 'dev']:
        imagePullPolicy = "Always"
    else:
        imagePullPolicy = "IfNotPresent"
    body = {
        'apiVersion': 'batch/v1',
        'kind': 'CronJob',
        'metadata': {
            'name': backup_name,
            'namespace': namespace,
            'annotations': {
                f'{annotation_namespace}/config': json.dumps(backup_config),
                f'{annotation_namespace}/owner-apiVersion': f'''{owner.api_version}''',
                f'{annotation_namespace}/owner-kind': f'''{owner.kind}''',
                f'{annotation_namespace}/owner-name': f'''{owner.metadata.name}''',
            },
            'labels': {
                'app.kubernetes.io/managed-by': annotation_namespace,
            },
        },
        'spec': {
            'schedule': backup_config['schedule'],
            'concurrencyPolicy': 'Replace',
            'suspend': False,
            'jobTemplate': {
                'spec': {
                    'backoffLimit': 2,
                    'template': {
                        'spec': {
                            'volumes': volumes,
                            'containers': [
                                {
                                    'name': 'backup-agent',
                                    'image': image,
                                    'imagePullPolicy': imagePullPolicy,
                                    'command': ['/bin/bash', '-c'],
                                    'args': args,
                                    'env': env,
                                    'volumeMounts': volumeMounts,
                                },
                            ],
                            'restartPolicy': 'OnFailure'
                        }
                    }
                }
            },
            'successfulJobsHistoryLimit': 2,
            'failedJobsHistoryLimit': 1,
        }
    }

    return body


def patch_cronjob(cronjob, backup_config={}, owner=None):
    namespace = cronjob['metadata']['namespace']
    # log.debug(f'''Patching CronJob with backup_config:\n{yaml.dump(backup_config)}''')
    body = render_cronjob_body(
        backup_config=backup_config,
        namespace=namespace,
        owner=owner
    )
    # log.debug(f'''Patching CronJob with body:\n{yaml.dump(body)}''')
    v1 = client.BatchV1Api()
    try:
        v1.patch_namespaced_cron_job(
            cronjob['metadata']['name'],
            namespace=namespace,
            body=body,
        )
    except ApiException as e:
        print("Exception when calling BatchV1Api->patch_namespaced_cron_job: %s\n" % e)
        raise
