import logging
import os

# Configure logging
logging.basicConfig(
    format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s',
)
log = logging.getLogger("backup-server")
try:
    log.setLevel(os.environ.get('LOG_LEVEL', 'DEBUG').upper())
except:
    log.setLevel('DEBUG')

annotation_namespace = 'decentci.backups'

## If INCLUDE_NAMESPACES is set, ignore IGNORED_NAMESPACES. Otherwise, 
## populate IGNORED_NAMESPACES.
ignored_namespaces = []
include_namespaces = []
include_namespaces_str = os.environ.get('INCLUDE_NAMESPACES', '')
if include_namespaces_str:
    include_namespaces = include_namespaces_str.split(',')
else:
    ignored_namespaces_str = os.environ.get('IGNORED_NAMESPACES', '')
    if ignored_namespaces_str:
        ignored_namespaces = ignored_namespaces_str.split(',')

def get_backup_agent_name(backup_name):
    return f'''{backup_name}'''
