from kubernetes import client
from kubernetes.client.rest import ApiException
import json
from globals import log, annotation_namespace


# def pvc_exists(namespace, name):
#     v1 = client.CoreV1Api()
#     pv = v1.read_namespaced_persistent_volume_claim(name, namespace)
#     log.debug(pv)
#     if pv:
#         return True
#     else:
#         return False


def get_pvc_by_name(name, namespace='default'):
    pvc_list = get_backup_pvc_list(namespace)
    for pv in pvc_list:
        if name == pv['metadata']['name']:
            return pv
    return None


def get_backup_pvc_list(namespace='default'):
    v1 = client.CoreV1Api()
    ret = v1.list_namespaced_persistent_volume_claim(
        namespace=namespace, _preload_content=False)
    pvc_list = json.loads(ret.data)
    # log.debug(pvc_list)
    return [item for item in pvc_list["items"]
            if 'labels' in item['metadata']
            and 'app.kubernetes.io/managed-by' in item['metadata']['labels']
            and item['metadata']['labels']['app.kubernetes.io/managed-by'] == annotation_namespace]


def create_namespaced_pvc(backup_config={}, namespace='default', owner=None):
    pvc_body = render_pvc_body(
        backup_config=backup_config, namespace=namespace, owner=owner)
    assert pvc_body
    name = pvc_body['metadata']['name']
    if get_pvc_by_name(name=name, namespace=namespace):
        log.warning(f"PVC {name} already exists. Nothing to create.")
    else:
        v1 = client.CoreV1Api()
        ret = v1.create_namespaced_persistent_volume_claim(namespace=namespace, body=pvc_body, pretty=True,
                                                           _preload_content=False, async_req=False)
        ret_dict = json.loads(ret.data)
        log.debug(f'''"{name}" PVC created.''')


def delete_namespaced_pvc(namespace='default', name=None):
    assert name
    if not get_pvc_by_name(name=name, namespace=namespace):
        log.warning(f"PVC {name} not found. Nothing to delete.")
    else:
        v1 = client.CoreV1Api()
        ret = v1.delete_namespaced_persistent_volume_claim(
            name=name, namespace=namespace, _preload_content=False, async_req=False)
        ret_dict = json.loads(ret.data)
        log.debug(f'''Deleting PVC "{name}" ...''')


def render_pvc_body(backup_config={}, namespace='default', owner=None):
    backup_name = backup_config['backup_name']
    body = {
        'apiVersion': 'v1',
        'kind': 'PersistentVolumeClaim',
        'metadata': {
            'name': backup_name,
            'namespace': namespace,
            'annotations': {
                f'{annotation_namespace}/config': json.dumps(backup_config),
                f'{annotation_namespace}/owner-apiVersion': f'''{owner.api_version}''',
                f'{annotation_namespace}/owner-kind': f'''{owner.kind}''',
                f'{annotation_namespace}/owner-name': f'''{owner.metadata.name}''',
            },
            'labels': {
                'app.kubernetes.io/managed-by': annotation_namespace,
            },
        },
        'spec': {
            'accessModes': [
                'ReadWriteMany'
            ],
            'resources': {
                'requests': {
                    'storage': '1Gi'
                }
            },
            'selector': {
                'matchLabels': {
                    'name': backup_name
                }
            },
            'storageClassName': '',
            'volumeName': backup_name
        },
    }
    return body

def patch_pvc(pvc, backup_config={}, owner=None):
    log.debug(f'''Updating PVC for "{backup_config['backup_name']}".''')
    namespace = pvc['metadata']['namespace']
    v1 = client.CoreV1Api()
    try:
        v1.patch_namespaced_persistent_volume_claim(pvc['metadata']['name'], namespace=namespace,
            body=render_pvc_body(backup_config=backup_config, namespace=namespace, owner=owner))
    except ApiException as e:
        print("Exception when calling CoreV1Api->patch_namespaced_persistent_volume_claim: %s\n" % e)
        raise
