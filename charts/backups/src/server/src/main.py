from kubernetes import client, config, watch
from kubernetes.client.rest import ApiException
from kubernetes.client import ApiException
from requests import HTTPError
import urllib3
import yaml
import json
import os
import sys
import time
from globals import log, annotation_namespace, ignored_namespaces, include_namespaces, get_backup_agent_name
from pod import get_agent_pod_list, delete_namespaced_pod, create_namespaced_pod, delete_namespaced_pod, get_pod_by_name, patch_agent
from cronjob import get_backup_cronjob_list, create_namespaced_cron_job, delete_namespaced_cron_job, render_cronjob_body, get_cronjob_by_name, patch_cronjob
from persistentvolume import delete_pv, create_pv, get_pv_by_name, get_backup_pv_list, patch_pv
from persistentvolumeclaim import delete_namespaced_pvc, create_namespaced_pvc, get_pvc_by_name, get_backup_pvc_list, patch_pvc
import copy
# import sys

backup_config_init = {
    'enabled': False,
    'agent_enabled': False,
    'backup_name': '',
    'schedule': os.environ.get('BACKUP_SCHEDULE'),
    'retention_policy': os.environ.get('RETENTION_POLICY'),
    'pvc_name_src': '',
    'pvc_name_backup': '',
    'nfs_server': '',
    'nfs_path': '',
    'database': {},
    'alerts': {},
    'engine': 'duplicati',
    'restic': {
        'repo': {
            'existingSecret': "restic-auth",
            'passwordKey': "restic-repo-password",
            'configFileKey': "",
            'resticRepositoryBase': "",
            'forgetArgs': "--keep-last=1 --keep-hourly=24 --keep-daily=7 --keep-weekly=4 --keep-monthly=6",
        },
        'server': {
            'baseurl': "",
            'protocol': "https",
            'existingSecret': "restic-auth",
            'usernameKey': "rest-server-username",
            'passwordKey': "rest-server-password",
        },
    }
}
def get_owner_by_name(name='', namespace='default', kind='Deployment'):
    appsV1Api = client.AppsV1Api()
    if kind == 'Deployment':
        ret = appsV1Api.list_namespaced_deployment(
            namespace, field_selector=f'''metadata.name={name}''', limit=1)
    elif kind == 'StatefulSet':
        ret = appsV1Api.list_namespaced_stateful_set(
            namespace, field_selector=f'''metadata.name={name}''', limit=1)
    elif kind == 'ConfigMap':
        ret = client.CoreV1Api().list_namespaced_config_map(
            namespace, field_selector=f'''metadata.name={name}''', limit=1)
    if ret and ret.items:
        ## Only one object is allowed to own a particular backup utility agent.
        return ret.items[0]
    else:
        return None

def get_annotation_value(label='', annotations={}):
    value = [val for key, val in annotations.items() if key == label]
    if value:
        return value[0]
    else:
        return None


def is_managed_persistent_volume(backup_config):
    assert valid_backup_config(backup_config)
    ## If backup engine is Restic, assume persistent volume is not used
    return backup_config['engine'] == 'duplicati' and backup_config['nfs_server'] and backup_config['nfs_path']


def create_persistent_volume(backup_config, namespace='default', owner=None):
    if not is_managed_persistent_volume(backup_config):
        return
    log.debug(f'''Creating backup PV/PVC pair: "{namespace}/{backup_config['backup_name']}"''')
    create_pv(backup_config=backup_config, owner=owner)
    create_namespaced_pvc(backup_config=backup_config, namespace=namespace, owner=owner)


def delete_persistent_volume_pair(backup_config={}, namespace='default', name=''):
    '''Delete the PV/PVC pair. If backup config is provided, only delete if
    the backup volume is managed by the backup system. If a name is directly
    provided, try to delete PV/PVC by that name.
    '''
    if backup_config:
        if not is_managed_persistent_volume(backup_config):
            return
        name = backup_config['backup_name']
    else:
        assert name
    log.debug(f'''Destroying backup PV/PVC pair: "{namespace}/{name}"''')
    delete_namespaced_pvc(namespace=namespace, name=name)
    delete_pv(name=name)
    ## The PV/PVC pair takes time to delete, so poll until deleted
    timeout = 60
    idx = 0
    while get_pv_by_name(name=name) or get_pvc_by_name(name=name, namespace=namespace):
        time.sleep(1)
        idx += 1
        if idx > timeout:
            break
    if get_pv_by_name(name=name) or get_pvc_by_name(name=name, namespace=namespace):
        log.warning(f'''PV/PVC pair "{namespace}/{name}" is taking a long time to delete.''')


def parse_configmap(configmap_object):
    '''Parse a ConfigMap object and extract the backup config'''
    global_config = {}
    try:
        ## Parse global config options first
        data = configmap_object.data
        if 'global' in data:
            global_config = yaml.load(data['global'], Loader=yaml.SafeLoader)
        else:
            log.debug(f'''No global_config detected.''')
    except Exception as e:
        log.error(f'''Failed to parse global config options: {e}''')
        return None
    try:
        assert data['backups']
    except AssertionError as e:
        log.error(f'''There must be a 'backups' key. 'global' key is optional. {e}''')
        return None
    ## Parse and validate backup config list
    backup_configs = []
    backup_config_list = yaml.load(data['backups'], Loader=yaml.SafeLoader)
    for backup_config_cm in backup_config_list:
        backup_config = process_backup_config(backup_config_cm=backup_config_cm, global_config=global_config)
        if valid_backup_config(backup_config=backup_config):
            backup_configs.append(backup_config)
    return backup_configs


def find_orphans(kind='',namespace='default'):
    orphans = []
    if kind == 'Pod':
        object_list = get_agent_pod_list(namespace=namespace)
    elif kind == 'CronJob':
        object_list = get_backup_cronjob_list(namespace=namespace)
    elif kind == 'PersistentVolume':
        object_list = get_backup_pv_list(namespace=namespace)
    elif kind == 'PersistentVolumeClaim':
        object_list = get_backup_pvc_list(namespace=namespace)
    else:
        log.warning(f'''Invalid orphan kind: "{kind}".''')
        return orphans
    for object in object_list:
        try:
            ## Find the object (Deployment/StatefulSet) that owns the asset
            owner_kind = get_annotation_value(label=f'{annotation_namespace}/owner-kind', annotations=object['metadata']['annotations'])
            owner_name = get_annotation_value(label=f'{annotation_namespace}/owner-name', annotations=object['metadata']['annotations'])
            owner = get_owner_by_name(
                name=owner_name,
                kind=owner_kind,
                namespace=namespace,
            )
            ## If the owner is missing or the owner does not claim the asset then the asset is orphaned
            is_orphan = True
            if owner_kind == 'ConfigMap':
                ## If the owner is a ConfigMap, parse the ConfigMap and verify the object is in 
                ## the list of backup configs
                for backup_config in parse_configmap(owner):
                    # log.debug(f'''(find_orphans) Analyzing backup "{backup_config['backup_name']}" for object "{kind}/{object['metadata']['name']}"''')
                    if backup_config['backup_name'] == object['metadata']['name']:
                        is_orphan = False
                        break
            else:
                ## If the owner is a Deplyment or StatefulSet, match the backup name to the
                ## owner annotations.
                owner_backup_name = get_annotation_value(label=f'''{annotation_namespace}/backup-name''', annotations=owner.metadata.annotations)
                if object['metadata']['name'] == owner_backup_name:
                    is_orphan = False
            if not owner or is_orphan:
                log.debug(f'''(find_orphans) Orphan detected: "{kind}/{object['metadata']['name']}"''')
                orphans.append(object)

        except Exception as e:
            log.error(f'''Error reading backup asset: {e}''')
    return orphans

def collect_garbage(namespace='default'):
    log.debug(f'''Performing garbage collection...''')
    ## Look for orphaned utility agents and delete them
    orphaned_agents = find_orphans(kind='Pod', namespace=namespace)
    for orphan in orphaned_agents:
        log.debug(f'''Destroying backup utility agent: "{orphan['metadata']['name']}"...''')
        delete_namespaced_pod(namespace=namespace, name=orphan['metadata']['name'])

    ## Look for orphaned backup CronJobs and delete them
    orphaned_cronjobs = find_orphans(kind='CronJob', namespace=namespace)
    for orphan in orphaned_cronjobs:
        log.debug(f'''Destroying backup CronJob: "{orphan['metadata']['name']}"...''')
        delete_namespaced_cron_job(namespace=namespace, name=orphan['metadata']['name'])

    ## Look for orphaned PV/PVCs and delete them
    orphaned_pvcs = find_orphans(kind='PersistentVolumeClaim', namespace=namespace)
    for orphan in orphaned_pvcs:
        ## The PV is assumed to always share the same name as the PVC that has been orphaned
        log.debug(f'''Orphan PVC found. Destroying "{orphan['metadata']['name']}"...''')
        delete_persistent_volume_pair(namespace=namespace, name=orphan['metadata']['name'])


def must_recreate(old: dict, new: dict, kind: str):
    '''Return True if the differences between input configs demand object recreation.'''
    ## If the environment variables in a pod spec need to be updated, a Kubernetes API patch
    ## will not work; the pod must be restarted to receive the updated env var values.
    if kind.lower() in ['pod', 'cronjob']:
        ## The list of keys are config parameters passed to the pod via env vars
        for key in ['schedule', 'retention_policy', 'database', 'alerts']:
            if old[key] != new[key]:
                return True
    return False

def identical_configs(config_1: dict, config_2: dict):
    '''Return True if input configs are not identical'''
    return json.dumps(config_1, sort_keys=True) == json.dumps(config_2, sort_keys=True)


def update_backup(backup_config=None, owner=None):
    '''Determine if the backup is enabled for the application and update as necessary'''
    try:
        assert valid_backup_config(backup_config)
    except Exception as e:
        log.error(f'''Invalid backup config: {e}''')
    try:
        namespace = owner.metadata.namespace
    except Exception as e:
        log.error(f'''Invalid event object: {owner}''')
        return
    backup_name = backup_config['backup_name']
    log.debug(f'''Analyzing backup "{namespace}/{backup_name}"''')

    ## CronJob
    create_cronjob = False
    delete_cronjob = False
    recreate_cronjob = False
    ## Backup agent Pod
    create_agent_pod = False
    delete_agent_pod = False
    recreate_agent_pod = False
    ## PV/PVC pair
    create_volume_pair = False
    delete_volume_pair = False
    recreate_volume_pair = False

    ## Delete asset if it is marked for deletion or if config has changed
    try:
        # log.debug(f'''Incoming backup config:\n{yaml.dump(backup_config)}''')
        ## Check if CronJob should be recreated
        backup_cronjob = get_cronjob_by_name(backup_name, namespace=namespace)
        if backup_cronjob:
            if backup_config['enabled']:
                cronjob_backup_config = json.loads(get_annotation_value(label=f'{annotation_namespace}/config', annotations=backup_cronjob['metadata']['annotations']),)
                recreate_cronjob = not identical_configs(backup_config, cronjob_backup_config)
                if recreate_cronjob:
                    # log.debug(f'''Recreating CronJob with current config:\n{yaml.dump(cronjob_backup_config)}''')
                    log.debug(f'''CronJob "{namespace}/{backup_name}" needs to be patched.''')
            else:
                delete_cronjob = True
        elif backup_config['enabled']:
            create_cronjob = True

        ## Check if backup agent should be recreated
        agent_pod = get_pod_by_name(backup_name, namespace=namespace)
        if agent_pod:
            if backup_config['agent_enabled']:
                agent_pod_backup_config = json.loads(get_annotation_value(label=f'{annotation_namespace}/config', annotations=agent_pod['metadata']['annotations']),)
                # log.debug(f'''Existing backup config from agent:\n{yaml.dump(agent_pod_backup_config)}''')
                recreate_agent_pod = not identical_configs(backup_config, agent_pod_backup_config)
                if recreate_agent_pod:
                    # log.debug(f'''Recreating agent Pod with current config:\n{yaml.dump(agent_pod_backup_config)}''')
                    log.debug(f'''Agent Pod "{namespace}/{backup_name}" needs to be patched.''')
            else:
                delete_agent_pod = True
        elif backup_config['agent_enabled']:
            create_agent_pod = True

        ## Check if PV/PVC pair should be recreated
        volume_pair_is_needed = backup_config['enabled'] or backup_config['agent_enabled']
        pv = get_pv_by_name(name=backup_name)
        pvc = get_pvc_by_name(name=backup_name, namespace=namespace)
        if pv and pvc:
            if volume_pair_is_needed:
                pv_backup_config = json.loads(get_annotation_value(label=f'{annotation_namespace}/config', annotations=pv['metadata']['annotations']),)
                pvc_backup_config = json.loads(get_annotation_value(label=f'{annotation_namespace}/config', annotations=pvc['metadata']['annotations']),)
                recreate_pv = not identical_configs(backup_config, pv_backup_config)
                recreate_pvc = not identical_configs(backup_config, pvc_backup_config)
                recreate_volume_pair = recreate_pv or recreate_pvc
                if recreate_volume_pair:
                    # log.debug(f'''Recreating PVC with current config:\n{yaml.dump(pvc_backup_config)}''')
                    # log.debug(f'''Recreating PV with current config:\n{yaml.dump(pv_backup_config)}''')
                    log.debug(f'''PV/PVC pair "{namespace}/{backup_name}" needs to be patched.''')
            else:
                delete_volume_pair = True
        elif volume_pair_is_needed:
            create_volume_pair = True

        ## Attempt to patch assets instead of deleting and recreating
        if recreate_cronjob:
            # log.debug(f'''Target backup config for {namespace}/{backup_name}:\n{yaml.dump(backup_config)}''')
            try:
                ## Check for changes that are not addressed by a patch
                if must_recreate(new=backup_config, old=cronjob_backup_config, kind='cronjob'):
                    delete_cronjob = True
                    create_cronjob = True
                else:
                    ## Attempt to patch object
                    patch_cronjob(backup_cronjob, backup_config=backup_config, owner=owner)
                    log.debug(f'''Patched CronJob for {namespace}/{backup_name}''')
            except ApiException as e:
                log.error(f"Error patching CronJob for {namespace}/{backup_name}: {e}")
            except Exception as e:
                log.error(f"Error updating CronJob for {namespace}/{backup_name}: {e}")
        if recreate_agent_pod:
            try:
                ## Check for changes that are not addressed by a patch
                if must_recreate(new=backup_config, old=agent_pod_backup_config, kind='pod'):
                    delete_agent_pod = True
                    create_agent_pod = True
                else:
                    ## Attempt to patch object
                    patch_agent(agent_pod, backup_config=backup_config, owner=owner)
                    log.debug(f'''Patched agent Pod for {namespace}/{backup_name}''')
            except ApiException as e:
                log.error(f"Error patching Pod for {namespace}/{backup_name}: {e}")
            except Exception as e:
                log.error(f"Error updating Pod for {namespace}/{backup_name}: {e}")
        ## If either of the patches fail for the PV/PVC pair, recreate both
        if recreate_volume_pair:
            try:
                ## Attempt to patch objects
                patch_pvc(pvc, backup_config=backup_config, owner=owner)
                log.debug(f'''Patched PVC for {namespace}/{backup_name}''')
            except Exception as e:
                ## If the patches fail, delete the objects
                log.error(f"Unable to patch PVC: {e}")
                delete_volume_pair = True
                create_volume_pair = True
            try:
                ## Attempt to patch objects
                patch_pv(pv, backup_config=backup_config, owner=owner)
                log.debug(f'''Patched PV for {namespace}/{backup_name}''')
            except Exception as e:
                ## If the patches fail, delete the objects
                log.error(f"Unable to patch PV: {e}")
                delete_volume_pair = True
                create_volume_pair = True

        ## Delete assets that need to be removed or recreated
        if delete_volume_pair:
            ## If the volume pair needs to be recreated, the agent pod must be recreated in order to 
            ## unbind the volume
            delete_agent_pod = True
            ## Only create the agent pod if it is supposed to exist
            create_agent_pod = create_agent_pod or recreate_agent_pod
        ## Delete the agent pod BEFORE deleting the volume pair
        if delete_agent_pod:
            delete_namespaced_pod(namespace=namespace, name=backup_name)
            ## Monitor deletion, which can take a long time
            timeout = 60
            idx = 0
            while True:
                agent_pod = get_pod_by_name(backup_name, namespace=namespace)
                if agent_pod:
                    time.sleep(1)
                    idx += 1
                    if idx > timeout:
                        break
                    continue
                else:
                    break
            agent_pod = get_pod_by_name(backup_name, namespace=namespace)
            if agent_pod:
                log.error(f'''Backup agent pod for "{namespace}/{backup_name}" is taking a long time to delete.''')
        if delete_volume_pair:
            delete_persistent_volume_pair(backup_config, namespace=namespace)
            ## Monitor deletion, which can take a long time
            timeout = 60
            idx = 0
            while True:
                pvc = get_pvc_by_name(backup_name, namespace=namespace)
                pv = get_pv_by_name(backup_name)
                if pvc or pv:
                    time.sleep(1)
                    idx += 1
                    if idx > timeout:
                        break
                    continue
                else:
                    break
            pvc = get_pvc_by_name(backup_name, namespace=namespace)
            pv = get_pv_by_name(backup_name)
            if pvc or pv:
                log.error(f'''Backup volume PV/PVC pair for "{namespace}/{backup_name}" is taking a long time to delete.''')
        if delete_cronjob:
            delete_namespaced_cron_job(namespace=namespace, name=backup_name)
            ## Monitor deletion, which can take a long time
            timeout = 60
            idx = 0
            while True:
                backup_cronjob = get_cronjob_by_name(backup_name, namespace=namespace)
                if backup_cronjob:
                    time.sleep(1)
                    idx += 1
                    if idx > timeout:
                        break
                    continue
                else:
                    break
            backup_cronjob = get_cronjob_by_name(backup_name, namespace=namespace)
            if backup_cronjob:
                log.error(f'''Backup CronJob for "{namespace}/{backup_name}" is taking a long time to delete.''')

        ## Create all configured assets as needed
        ##
        if create_cronjob:
            ## Create backup cronjob
            log.debug(f'''Creating backup cronjob: "{backup_name}".''')
            create_namespaced_cron_job(backup_config=backup_config, namespace=namespace, owner=owner)
        if create_agent_pod:
            ## Create utility agent
            log.debug(f'''Creating backup utility agent: "{backup_name}".''')
            create_namespaced_pod(backup_config, namespace=namespace, owner=owner)
        if create_volume_pair:
            ## Create persistent volume PV/PVC pair
            create_persistent_volume(backup_config, namespace=namespace, owner=owner)
    except Exception as e:
        log.error(f'''Error updating backup assets: {e}''')

def backup_config_key_map(key):
    key_map = {
        'enabled': 'enabled',
        'agent-enabled': 'agent_enabled',
        'backup-name': 'backup_name',
        'retention-policy': 'retention_policy',
        'schedule': 'schedule',
        'source-pvc': 'pvc_name_src',
        'backup-pvc': 'pvc_name_backup',
        'backup-nfs-server': 'nfs_server',
        'backup-nfs-path': 'nfs_path',
        'database': 'database',
        'alerts': 'alerts',
        'engine': 'engine',
        'restic': 'restic',
    }
    try:
        return key_map[key]
    except:
        return key

def merge_config(in_conf: dict = {}, override_conf: dict = {}, path: list = []) -> dict:
    conf = dict(in_conf)
    # log.debug(f'''path: {path}''')
    # log.debug(f'''conf: {json.dumps(conf, indent=2)}''')
    # log.debug(f'''override_conf: {json.dumps(override_conf, indent=2)}''')
    for key in override_conf:
        # log.debug(f'''key: {key}''')
        if key in conf:
            if isinstance(conf[key], dict) and isinstance(override_conf[key], dict):
                # log.debug(f'''Overriding dict value of key "{key}":\n{json.dumps(override_conf[key], indent=2)}''')
                appended_path = path + [str(key)]
                # log.debug(f'''appended path: {appended_path}''')
                conf[key] = merge_config(conf[key], override_conf[key], appended_path)
            elif conf[key] == override_conf[key]:
                pass
            else:
                # log.debug(f'''Duplicate key "{key}" with conflicting value found. Overriding initial value.''')
                conf[key] = override_conf[key]
        else:
            conf[key] = override_conf[key]
    return conf

def process_backup_config(backup_config_cm={}, global_config={}):
    '''Construct a backup configuration from the ConfigMap data'''
    ## Initialize with default values
    backup_config = copy.deepcopy(backup_config_init)
    # log.debug(f'''Default config:\n{yaml.dump(backup_config)}''')
    ## Set values from global config
    ## Convert top-level key names to mapped values
    global_config_mapped = {}
    for key, value in global_config.items():
        global_config_mapped[backup_config_key_map(key)] = value
    ## Merge the global config by overriding the default config
    backup_config = merge_config(backup_config, global_config_mapped)
    # log.debug(f'''Global config merged:\n{yaml.dump(backup_config)}''')
    ## Convert top-level key names to mapped values
    backup_config_cm_mapped = {}
    for key, value in backup_config_cm.items():
        backup_config_cm_mapped[backup_config_key_map(key)] = value
    ## Override values with backup-specific config
    backup_config = merge_config(backup_config, backup_config_cm_mapped)
    # log.debug(f'''Processed backup config:\n{yaml.dump(backup_config)}''')
    return backup_config
    
def process_annotations(backup_annotations={}):
    '''Construct a backup configuration from the input set of annotations
       WARNING: Annotation-based backups are deprecated and will be unsupported
       in future versions.
    '''

    ## Initialize with default values
    backup_config = copy.deepcopy(backup_config_init)

    for label, value in backup_annotations.items():
        if label == f'''{annotation_namespace}/enabled''' and value.lower() in ['true', 'false']:
            backup_config['enabled'] = value.lower() == 'true'
        elif label == f'''{annotation_namespace}/agent-enabled''' and value.lower() in ['true', 'false']:
            backup_config['agent_enabled'] = value.lower() == 'true'
        elif label == f'''{annotation_namespace}/backup-name''':
            backup_config['backup_name'] = value
        elif label == f'''{annotation_namespace}/schedule''':
            backup_config['schedule'] = value
        elif label == f'''{annotation_namespace}/retention-policy''':
            backup_config['retention_policy'] = value
        elif label == f'''{annotation_namespace}/source-pvc''':
            backup_config['pvc_name_src'] = value
        elif label == f'''{annotation_namespace}/backup-pvc''':
            backup_config['pvc_name_backup'] = value
        elif label == f'''{annotation_namespace}/backup-nfs-server''':
            backup_config['nfs_server'] = value
        elif label == f'''{annotation_namespace}/backup-nfs-path''':
            backup_config['nfs_path'] = value
        elif label == f'''{annotation_namespace}/database''':
            backup_config['database'] = json.loads(value)
        elif label == f'''{annotation_namespace}/alerts''':
            backup_config['alerts'] = json.loads(value)
        elif label == f'''{annotation_namespace}/engine''':
            backup_config['engine'] = value
        elif label == f'''{annotation_namespace}/restic''':
            backup_config['restic'] = json.loads(value)
    log.debug(f'''Detected backup config:\n{yaml.dump(backup_config)}''')
    return backup_config


def valid_backup_config(backup_config):
    try:
        assert isinstance(backup_config['engine'], str)
        backup_config['engine'] = backup_config['engine'].lower()
        assert backup_config['engine'] in ['duplicati', 'restic']
    except AssertionError as e:
        log.error(f'''Invalid value for "engine". Acceptable values are "duplicati" or "restic". {e}''')
        return False
    try:
        assert isinstance(backup_config['enabled'], bool)
    except AssertionError as e:
        log.error(f'''Invalid boolean value for "enabled". {e}''')
        return False
    try:
        assert isinstance(backup_config['agent_enabled'], bool)
    except AssertionError as e:
        log.error(f'''Invalid boolean value for "agent-enabled". {e}''')
        return False
    try:
        assert isinstance(backup_config['retention_policy'], str)
    except AssertionError as e:
        log.error(f'''retention policy must be a string. {e}''')
        return False
    try:
        assert isinstance(backup_config['schedule'], str)
    except AssertionError as e:
        log.error(f'''backup schedule must be a string. {e}''')
        return False
    try:
        assert isinstance(backup_config['backup_name'], str)
    except AssertionError as e:
        log.error(f'''Backup name must be a string. {e}''')
        return False
    try:
        ## Only one must be set: pvc_name and/or database
        assert backup_config['pvc_name_src'] or backup_config['database']
        assert not (backup_config['pvc_name_src'] and backup_config['database'])
    except AssertionError as e:
        log.error(f'''Exactly one and only one must be specified: a source file PVC or a database config. {e}''')
        return False
    try:
        if backup_config['pvc_name_src']:
            ## If pvc_name is set, it must be a string
            assert isinstance(backup_config['pvc_name_src'], str)
        if backup_config['database']:
            ## If database is set, it must have the required fields
            assert backup_config['database']['type'] in ['mysql', 'postgresql']
            assert backup_config['database']['host']
            assert backup_config['database']['auth']['username']
            assert backup_config['database']['auth']['database']
            assert backup_config['database']['auth']['existingSecret']
            assert backup_config['database']['auth']['passwordKey']
    except AssertionError as e:
        log.error(f'''Invalid source PVC or database config. {e}''')
        return False
    try:
        if backup_config['alerts']:
            ## If alerts are configured, it must have the required fields
            assert backup_config['alerts']['matrix']['existingSecret']
            assert backup_config['alerts']['matrix']['urlKey']
    except AssertionError as e:
        log.error(f'''Invalid alerts config. {e}''')
        return False
    ## If the backup engine is duplicati, validate the target PVC or NFS location.
    ## Otherwise, the engine is restic and these options are ignored because the
    ## target is assumed to be a Restic server.
    if backup_config['engine'] == 'duplicati':
        try:
            # Either a PVC must be specified or an NFS target
            assert (
                backup_config['pvc_name_backup']
                and isinstance(backup_config['pvc_name_backup'], str)
            ) or (
                backup_config['nfs_path']
                and isinstance(backup_config['nfs_path'], str)
                and backup_config['nfs_server']
                and isinstance(backup_config['nfs_server'], str)
            )
        except AssertionError as e:
            log.error(f'''Invalid target backup PVC or NFS config. {e}''')
            return False
        try:
            assert not (backup_config['pvc_name_backup'] and backup_config['nfs_server'])
        except AssertionError as e:
            log.error(f'''Exactly one and only one must be specified: target backup PVC or NFS config. {e}''')
            return False
    ## If the backup engine is Restic, validate the restic repo config
    ## TODO: Be more detailed about what failed in the error messaging.
    if backup_config['engine'] == 'restic':
        try:
            assert isinstance(backup_config['restic']['repo']['existingSecret'], str) and backup_config['restic']['repo']['existingSecret']
            assert isinstance(backup_config['restic']['repo']['passwordKey'], str) and backup_config['restic']['repo']['passwordKey']
            assert isinstance(backup_config['restic']['repo']['forgetArgs'], str)
            ## If a config file is provided for a custom restic backup target, do not validate the restic server config
            if backup_config['restic']['repo']['configFileKey']:
                assert isinstance(backup_config['restic']['repo']['configFileKey'], str)
            else:
                assert isinstance(backup_config['restic']['server']['baseurl'], str) and backup_config['restic']['server']['baseurl']
                assert isinstance(backup_config['restic']['server']['protocol'], str) and backup_config['restic']['server']['protocol']
                assert isinstance(backup_config['restic']['server']['existingSecret'], str) and backup_config['restic']['server']['existingSecret']
                assert isinstance(backup_config['restic']['server']['passwordKey'], str) and backup_config['restic']['server']['passwordKey']
                assert isinstance(backup_config['restic']['server']['usernameKey'], str) and backup_config['restic']['server']['usernameKey']
        except AssertionError as e:
            log.error(f'''Invalid restic repo config. {e}''')
            return False
        
    # log.debug('Valid backup configuration.')
    return True

def process_configmap_stream_event(event):
    event_object = event["object"]
    name = event_object.metadata.name
    namespace = event_object.metadata.namespace
    ## If include lists is not empty, only watch explicitly included namespaces
    if include_namespaces and namespace not in include_namespaces:
        log.debug(f'''Skipping event in namespace "{namespace}" because not in include list.''')
        return
    ##  If include lists is empty, only skip objects in ignored namespaces
    elif not include_namespaces and namespace in ignored_namespaces:
        log.debug(f'''Skipping event in excluded namespace "{namespace}".''')
        return
    try:
        for backup_config in parse_configmap(event_object):
            try:
                ## Update the backup associated with the current object
                update_backup(backup_config=backup_config, owner=event_object)
            except Exception as e:
                log.error(f'''Error updating backup "{namespace}/{name}": {e}''')
    except Exception as e:
        log.error(f'''Failed to handle backup configmap: {e}''')
        return False
    try:
        ## Perform garbage collection
        collect_garbage(namespace=namespace)
    except Exception as e:
        log.error(f'''Failed to garbage collect: {e}''')
        return False
    

def process_stream_event(event):
    # event_type = event["type"]
    event_object = event["object"]

    # get information about Deployment
    name = event_object.metadata.name
    namespace = event_object.metadata.namespace
    # Skip objects in ignored namespaces
    if namespace in ignored_namespaces:
        log.debug(f'''Skipping event in excluded namespace "{namespace}"''')
        return
    annotations = event_object.metadata.annotations
    if not annotations:
        return

    backup_annotations = {}
    for label, annotation in annotations.items():
        if label.startswith(annotation_namespace):
            # log.debug(f'''Label: {label}, Annotation: {annotation}''')
            backup_annotations[label] = annotation
    if not backup_annotations:
        return

    backup_config = process_annotations(backup_annotations=backup_annotations)
    if not valid_backup_config(backup_config=backup_config):
        log.warning(f'''Invalid backup configuration: "{namespace}/{name}". Ignoring...''')
        return

    # ## Observe the differences between the current and previous event in the watch stream
    # log.debug(f'''resource_version: {resource_version}, event type: {event_type}''')
    # events.append(event)
    # if len(events) > 1:
    #     prev_event_obj_str = yaml.dump(events[-2]['raw_object'])
    #     event_obj_str = yaml.dump(events[-1]['raw_object'])
    #     # log.debug(prev_event_obj_str)
    #     # log.debug(event_obj_str)
    #     log.debug(f'''diff to previous event:''')
    #     log.debug(sys.stdout.writelines(
    #         difflib.unified_diff(prev_event_obj_str.splitlines(keepends=True), event_obj_str.splitlines(keepends=True))
    #     ))

    try:
        ## Update the backup associated with the current object
        update_backup(backup_config=backup_config, owner=event_object)
        ## Perform garbage collection
        collect_garbage(namespace=namespace)
    except Exception as e:
        log.error(f'''Error updating backup "{namespace}/{name}": {e}''')


def watch_stream(func_handler):
    resource_version = None
    while True:
        time.sleep(1)
        try:
            stream = watch.Watch().stream(func_handler, resource_version=resource_version)
            for event in stream:
                ## Record where to resume streaming.
                resource_version = event["object"].metadata.resource_version
                process_stream_event(event=event)

        except urllib3.exceptions.ProtocolError as e:
            log.error('KubeWatcher reconnecting to Kube API: %s' % str(e))
            if stream:
                stream.close()
            stream = None
            time.sleep(2)
            continue
        except (ApiException, HTTPError) as e:
            if stream:
                stream.close()
            stream = None
            log.error("Connection to kube API failed: " + str(e))
            if e.status == 410:
                # Resource too old
                resource_version = None
            time.sleep(2)
            continue

def watch_configmap_stream(func_handler):
    resource_version = None
    while True:
        time.sleep(1)
        try:
            stream = watch.Watch().stream(func_handler, resource_version=resource_version, label_selector='app.kubernetes.io/name=decentci-backups')
            for event in stream:
                ## Record where to resume streaming.
                resource_version = event["object"].metadata.resource_version
                process_configmap_stream_event(event=event)

        except urllib3.exceptions.ProtocolError as e:
            log.error('KubeWatcher reconnecting to Kube API: %s' % str(e))
            if stream:
                stream.close()
            stream = None
            time.sleep(2)
            continue
        except (ApiException, HTTPError) as e:
            if stream:
                stream.close()
            stream = None
            log.error("Connection to kube API failed: " + str(e))
            if e.status == 410:
                # Resource too old
                resource_version = None
            time.sleep(2)
            continue


def watch_deployments():
    appsV1Api = client.AppsV1Api()
    watch_stream(func_handler=appsV1Api.list_deployment_for_all_namespaces)

def watch_statefulsets():
    appsV1Api = client.AppsV1Api()
    watch_stream(func_handler=appsV1Api.list_stateful_set_for_all_namespaces)

def watch_configmaps():
    CoreV1Api = client.CoreV1Api()
    watch_configmap_stream(func_handler=CoreV1Api.list_config_map_for_all_namespaces)


def main():
    """
    Watch Deployments and StatefulSets and create/delete backup CronJobs 
    if their annotations include a backup configuration
    """
    try:
        config.load_incluster_config()
    except:
        config.load_kube_config()

    supported_kinds = {
        'deployment': watch_deployments,
        'statefulset': watch_statefulsets,
        'configmap': watch_configmaps,
    }
    try:
        kind_to_watch = sys.argv[1]
    except Exception as e:
        log.error(f'''Error getting script argument: {e}''')
        return
    if kind_to_watch not in supported_kinds:
        log.error(f'''Invalid kind "{kind_to_watch}". Supported kinds are: {json.dumps([key for key,val in supported_kinds.items()])}''')
        return
    try:
        ## Run watcher function for specified object kind
        func_handle = supported_kinds[kind_to_watch]
        func_handle()
    except Exception as e:
        log.error(f'''Error running watcher for "{kind_to_watch}": {e}''')

if __name__ == '__main__':
    main()
