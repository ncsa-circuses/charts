# Backup agent

## Restic configuration

The restic backup repo configuration requires two environment variables: `RESTIC_PASSWORD` and `RESTIC_REPOSITORY`. See [the Restic documentation for more details](https://restic.readthedocs.io/en/stable/040_backup.html?highlight=environment%20variables#environment-variables).

## Matrix alerts

Provide a Matrix alert URL via a Kubernetes Secret of the form:

```yaml
matrix-url: 'https://matrix.musesframework.io/_matrix/client/r0/rooms/%21dXTuZfHJLefRHfblab:musesframework.io/send/m.room.message?access_token=syt_UYhubmluZ2E_agFidoDmkQPoFQBsZtpR_19HSL9'
```
## Testing

Build and run the tests:

```bash
docker build . -t registry.gitlab.com/decentci/charts/data-backup-agent:dev
```

To test the backup script against a local, ephemeral backup repo, construct a config file similar to this:

```bash
cat > /tmp/restic.conf << EOF

export RESTIC_REPOSITORY="/backup"
export RESTIC_PASSWORD="5a53320e359c29767ac408118ca4287b3aa36824"

EOF
```

To test the backup script against a remote backup repo, construct a config file similar to this:

```bash
cat > /tmp/restic.conf << EOF

export RESTIC_REPOSITORY="s3:https://s3.example.com/my-backup-bucket/restic-backups"
export RESTIC_PASSWORD="5a53320e359c29767ac408118ca4287b3aa36824"
export AWS_ACCESS_KEY_ID="ABCitsaseasy"
export AWS_SECRET_ACCESS_KEY="as123"

EOF
```

Mount this config file into the container and source it prior to running the test script.

```bash
docker run --rm -it \
     -v /tmp/restic.conf:/etc/restic.conf \
     registry.gitlab.com/decentci/charts/data-backup-agent:dev \
     bash -c ' \
          source /etc/restic.conf && \
          bash test_backups_restic.sh && \
          restic ls latest && \
          restic stats \
     '
```
