#!/bin/bash

set -e

## This test script is intended to be run only when
## testing offline, where /backup and /data are not 
## mounted.

## Only the restic backup engine is supported.
export BACKUP_ENGINE="restic"

## Generate dummy data
mkdir /data || exit 1
echo "Generating dummy data in /data..."
idx=1
while (($idx < 11)); do
    echo $(openssl rand -hex $idx) > $(mktemp --tmpdir=/data)
    idx=$((idx+1))
done
## Display generated file contents
find /data -type f -print0 | 
  xargs -0 sh -c '
      for file; do
          echo "$(readlink -f ${file}): $(cat ${file})"
      done
  ' sh

restic_list_backup_files() {
    echo "Listing files in latest snapshot:"
    restic ls latest
}

echo -e "\n\n-----------------------------------------------------";
echo "Run initial flat file backup..."
bash run_backup.sh files "/var/log/decentci_backup.1.log"
restic_list_backup_files

echo -e "\n\n-----------------------------------------------------";
echo "Running again with no source data changes..."
bash run_backup.sh files "/var/log/decentci_backup.2.log"
restic_list_backup_files

echo -e "\n\n-----------------------------------------------------";
echo "Running again to test source data changed..."
echo "one new file" > /data/new_file.txt
bash run_backup.sh files "/var/log/decentci_backup.3.log"
echo "Listing differences between latest two snapshots:"
restic diff $(restic snapshots --json | jq -r '.[-2:][].id')
restic_list_backup_files

# echo -e "\n\n-----------------------------------------------------";
# echo "Running again to test repair when db file is missing and no source data changed..."
# set -x
# rm -rf "/backup/data/"
# set +x
# bash run_backup.sh files "/var/log/decentci_backup.4.log"
# restic_list_backup_files
