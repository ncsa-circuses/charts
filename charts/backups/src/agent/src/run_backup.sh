#!/bin/bash
set -e

## Environment variable BACKUP_ENGINE must be set to "duplicati" or "restic".

if [[ "$1" == "" ]]; then
    echo "Usage: $0 files|db"
    exit 1
fi
if [[ "x$2" == "x" ]]; then
    LOG_FILE="/var/log/decentci_backup.log"
else
    LOG_FILE="$2"
fi
if [[ "$1" == "db" ]]; then
    echo "INFO: Running a SQL database backup..."
    bash "backup_${BACKUP_ENGINE}.sh" db "${LOG_FILE}" 2>&1 | tee "${LOG_FILE}"
fi
if [[ "$1" == "files" ]]; then
    echo "INFO: Running a flat file backup..."
    bash "backup_${BACKUP_ENGINE}.sh" files "${LOG_FILE}" 2>&1 | tee "${LOG_FILE}"
fi

if ! grep --quiet 'DecentCI backup complete.' "${LOG_FILE}"; then
    echo "WARNING: Sending alert for failed backup..."
    log_text=$(cat "${LOG_FILE}")
    log=$(cat <<EOF
Backup failure: "${BACKUP_NAME}"
Log:
\`\`\`
${log_text}
\`\`\`
EOF
)
    log_formatted="Backup failure: \"${BACKUP_NAME}\"<br>Log:<br><pre><code>"${log_text}"</code></pre>"
    PAYLOAD="$(jq --null-input \
        --arg name "${BACKUP_NAME}" \
        --arg log "${log}" \
        --arg log_formatted "${log_formatted}" \
        '{"msgtype": "m.text", "format": "org.matrix.custom.html", "body": $log, "formatted_body": $log_formatted}' \
    )"

    # echo ${PAYLOAD}
    bash send_alerts.sh "${PAYLOAD}"
fi
