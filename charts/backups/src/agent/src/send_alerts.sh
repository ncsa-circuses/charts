#!/bin/bash
set -e

## If no Matrix alert URL is configured
if [[ "${MATRIX_ALERT_URL}x" == "x" ]]; then
  exit 0
fi

PAYLOAD=$1
curl --silent -XPOST -d "${PAYLOAD}" "${MATRIX_ALERT_URL}"
