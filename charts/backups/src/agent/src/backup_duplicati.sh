#!/bin/bash
set -e

if [[ "$1" == "" || "$2" == "" ]]; then
    echo "Usage: $0 files|db [log file path]"
    exit 1
fi

LOG_FILE="$2"

## Copy or initialize Duplicati database file
dbfile="${BACKUP_NAME}.sqlite"
dbpath="/opt/duplicati/${dbfile}"
tmpdbdir="/tmp/duplicati"
tmpdbpath="${tmpdbdir}/${dbfile}"
mkdir -p "${tmpdbdir}"
if [[ -f "${dbpath}" ]]; then
    echo "Copying Duplicati database file from persistent storage..."
    rsync -a "${dbpath}" "${tmpdbpath}"
else
    echo "Initializing Duplicati database file for first backup..."
    touch "${tmpdbpath}"
fi

## If this is a flat file backup, set the source data directory.
if [[ "$1" == "files" ]]; then
    SOURCE_DIR="/data"
fi
## If this is a SQL database backup, dump the database.
if [[ "$1" == "db" ]]; then
    SOURCE_DIR="/tmp/sqldump"
    mkdir -p $SOURCE_DIR
    BACKUP_DUMP_FILE="${SOURCE_DIR}/${DB_NAME}.sql"
    # Dump the database to a file
    if [[ $DB_TYPE == "postgresql" ]]; then
    # ref: https://www.postgresql.org/docs/current/app-pgdump.html
    export PGPASSWORD="$DB_PASS"
    pg_dump --clean --no-acl --host=$DB_HOST --dbname=$DB_NAME --username=$DB_USER > ${BACKUP_DUMP_FILE}
    fi
    if [[ $DB_TYPE == "mysql" ]]; then
    mysqldump --column-statistics=0 --host=$DB_HOST --user=$DB_USER --password=$DB_PASS $DB_NAME > ${BACKUP_DUMP_FILE}
    fi
fi

duplicati_backup() {
    ## Run Duplicati backup
    duplicati-cli backup \
        file:///backup/ \
        "${SOURCE_DIR}" \
        --dbpath="${tmpdbpath}" \
        --compression-module=zip \
        --dblock-size=100MB \
        --retention-policy="\"${RETENTION_POLICY}\"" \
        --no-encryption=true \
        --disable-module=console-password-input
    return $?
}

duplicati_repair() {
    ## Run Duplicati repair
    duplicati-cli repair \
        file:///backup/ \
        --rebuild-missing-dblock-files \
        --dbpath="${tmpdbpath}" \
        --compression-module=zip \
        --dblock-size=100MB \
        --retention-policy="\"${RETENTION_POLICY}\"" \
        --no-encryption=true \
        --disable-module=console-password-input
    return $?
}

set +e
duplicati_backup
duplicati_exit_code=$?
set -e
## Check for non-failure exit codes
## ref: https://github.com/duplicati/duplicati/issues/678#issuecomment-51173928
##     0 - Success
##     1 - Success (but no changed files)
##     2 - Completed by retried some files, or some files were locked (warnings)
##    50 - Some files were uploaded, then connection died
##   100 - No connection to server -> Fatal error
##   200 - Invalid command/arguments
if (( $duplicati_exit_code > 2 )); then
    ## Try to repair 
    if grep --quiet 'please run repair' "${LOG_FILE}"; then
        echo "Duplicati backup failed: exit code = ${duplicati_exit_code}. Attempting repair..."
        set +e
        duplicati_repair
        duplicati_exit_code=$?
        ## If the repair was successful, retry the backup
        repair_failed=0
        if grep 'Repair not possible' "${LOG_FILE}"; then
            repair_failed=1
        fi
        if (( $duplicati_exit_code > 0 )) ; then
            ## TODO: It is not clear that the repair exit code is useful, because it is
            ## zero even when repair is not possible.
            repair_failed=1
        fi
        if (( $repair_failed == 0 )); then
            echo "Repair was successful. Retrying backup..."
            duplicati_backup
            duplicati_exit_code=$?
        else
            echo "Duplicati repair failed: exit code = ${duplicati_exit_code}. Aborting..."
            exit 1
        fi
        set -e
    fi
fi
if (( $duplicati_exit_code > 2 )); then
    echo "Duplicati backup failed: exit code = ${duplicati_exit_code}. Aborting..."
    exit 1
else
    echo "Duplicati exit code = ${duplicati_exit_code}."
fi

## Copy updated Duplicati database file to permanent storage
set +e
echo "Copying Duplicati database file to persistent storage..."
rsync -av "${tmpdbpath}" "${dbpath}"
rsync_exit_code=$?
set -e

if (( $rsync_exit_code == 0 )); then
    echo "DecentCI backup complete."
else
    echo "Error copying Duplicati database file to persistent storage."
fi
