#!/bin/bash
set -e

## This test script is intended to be run only when
## testing offline, where /backup and /data are not 
## mounted.
mkdir -p /opt/restic /backup
mkdir /data || exit 1

if [[ "$1" == "reset" ]]; then
    echo "Deleting existing backup data..."
    rm -rf /opt/restic/*
    rm -rf /backup/*
fi

## Generate dummy data
echo "Generating dummy data in /data..."
idx=1
while (($idx < 11)); do
    echo $(openssl rand -hex $idx) > $(mktemp --tmpdir=/data)
    idx=$((idx+1))
done
## Display generated file contents
find /data -type f -print0 | 
  xargs -0 sh -c '
      for file; do
          echo "$(readlink -f ${file}): $(cat ${file})"
      done
  ' sh

duplicati_list_backup_files() {
    duplicati-cli find \
        file:///backup/ "*" \
        --no-encryption=true
}

## Run a flat file backup
export BACKUP_NAME=test-backup-files
echo "-----------------------------------------------------"; echo
bash run_backup.sh files "/var/log/decentci_backup.1.log"

echo "-----------------------------------------------------"; echo
echo "Running again with no source data changes..."
bash run_backup.sh files "/var/log/decentci_backup.2.log"
set +e
duplicati_list_backup_files
set -e

echo "-----------------------------------------------------"; echo
echo "Running again to test repair when db file is missing and no source data changed..."
rm /opt/restic/*
ls -lahn /opt/restic
ls -lahn /data
ls -lahn /backup
bash run_backup.sh files "/var/log/decentci_backup.3.log"

set +e
duplicati_list_backup_files
set -e

echo "-----------------------------------------------------"; echo
echo "Running again to test repair when db file is missing and source data changed..."
rm /opt/restic/*
ls -lahn /opt/restic
echo "one new file" > /data/new_file.txt
ls -lahn /data
ls -lahn /backup
bash run_backup.sh files "/var/log/decentci_backup.4.log"
set +e
duplicati_list_backup_files
set -e

echo "-----------------------------------------------------"; echo
echo "Running again to test repair when backup file is missing..."
rm $(find /backup/ -type f | tail -n1)
bash run_backup.sh files "/var/log/decentci_backup.5.log"
set +e
duplicati_list_backup_files
set -e
