#!/bin/bash
set -e

DUMP_FILE_PATH=$1
if [[ "x$DUMP_FILE_PATH" == "x" ]]; then
    echo "Usage: $0 [/path/to/sql/dump]"
    exit 1
fi
if [[ ! -f "$DUMP_FILE_PATH" ]]; then
    echo "File not found: \"$DUMP_FILE_PATH\""
    exit 1
fi

if [ -z "$DB_TYPE" ]; then
    echo "Environment variable DB_TYPE must be set to \"postgresql\" or \"mysql\"."
    exit 1
fi  

if [[ $DB_TYPE == "postgresql" ]]; then
    PGPASSWORD="${DB_PASS}" psql \
    --host=$DB_HOST \
    --dbname=$DB_NAME \
    --username=$DB_USER < "${DUMP_FILE_PATH}"
fi

if [[ $DB_TYPE == "mysql" ]]; then
    mysql \
    --host=$DB_HOST \
    --user=$DB_USER \
    --password="${DB_PASS}" \
    --database=$DB_NAME < "${DUMP_FILE_PATH}"
fi

echo "Database restore complete."
exit 0
