#!/bin/bash
set -e

if [[ "$1" == "" || "$2" == "" ]]; then
    echo "Usage: $0 files|db [log file path]"
    exit 1
fi

LOG_FILE="$2"

## If RESTIC_REPOSITORY is already set, do not override
if [[ -z "$RESTIC_REPOSITORY" ]]; then
    export RESTIC_REPOSITORY="rest:${RESTIC_SERVER_PROTOCOL}://${RESTIC_SERVER_USER}:${RESTIC_SERVER_PASS}@${RESTIC_SERVER_BASEURL}/${RESTIC_SERVER_USER}/${BACKUP_NAME}/"
fi
## If there is a restic repo config file of finite size, source it to set/override
## arbitrary env vars.
if [[ -s "/etc/restic.conf" ]]; then
    set -a
    source /etc/restic.conf
    set +a
fi

## Initialize Restic backup repo
if restic cat config >/dev/null 2>&1; then
  echo "INFO: Backup repo already initialized."
else
  echo "INFO: Initializing backup repo..."
  restic init
fi

## Check the restic repo health
set +e
echo "INFO: Checking the backup repo state..."
restic check --read-data-subset=2%
restic_exit_code=$?
set -e

if (( $restic_exit_code != 0 )); then
    echo "WARNING: Backup repo state check failed. Attempting to repair..."
    set +e
    restic repair index && restic repair snapshots
    restic_exit_code=$?
    set -e
    if (( $restic_exit_code == 0 )); then
        set +e
        restic check --read-data-subset=2%
        restic_exit_code=$?
        set -e
        if (( $restic_exit_code != 0 )); then
            echo "ERROR: Backup repo check still fails after repair operation. Aborting backup."
            exit 1
        else
            echo "INFO: Backup repo repair succeeded. Continuing with backup..."
        fi
    else
        echo "ERROR: Backup repo repair operation failed. Aborting backup."
        exit 1
    fi
fi

## If this is a flat file backup, set the source data directory.
if [[ "$1" == "files" ]]; then
    SOURCE_DIR="/data"
fi
## If this is a SQL database backup, dump the database.
if [[ "$1" == "db" ]]; then
    SOURCE_DIR="/tmp/sqldump"
    mkdir -p $SOURCE_DIR
    BACKUP_DUMP_FILE="${SOURCE_DIR}/${DB_NAME}.sql"
    # Dump the database to a file
    if [[ $DB_TYPE == "postgresql" ]]; then
    # ref: https://www.postgresql.org/docs/current/app-pgdump.html
    export PGPASSWORD="$DB_PASS"
    pg_dump --clean --no-acl --host=$DB_HOST --dbname=$DB_NAME --username=$DB_USER > ${BACKUP_DUMP_FILE}
    fi
    if [[ $DB_TYPE == "mysql" ]]; then
    mysqldump --column-statistics=0 --host=$DB_HOST --user=$DB_USER --password=$DB_PASS $DB_NAME > ${BACKUP_DUMP_FILE}
    fi
fi

restic_backup() {
    ## Run Restic backup
    restic backup \
        --cleanup-cache \
        --json \
        --host "${BACKUP_NAME}" \
        "${SOURCE_DIR}" \
        | jq 'select(.message_type != "status")'
    return $?
}

restic_forget_and_prune() {
    set -x
    restic forget --prune --json $RESTIC_FORGET_ARGS
    set +x
    return $?
}

set +e
echo "INFO: Running restic backup..."
restic_backup
restic_exit_code=$?
set -e

if (( $restic_exit_code != 0 )); then
    echo "ERROR: Backup failed."
    exit 1
fi

set +e
echo "INFO: Running restic forget and prune..."
restic_forget_and_prune
restic_exit_code=$?
set -e

if (( $restic_exit_code != 0 )); then
    echo "ERROR: Failed to prune repo."
    exit 1
fi

echo "INFO: DecentCI backup complete."
exit 0
