# Backups

## Overview

This backups chart aims to provide an easy, robust, and uniform way to backup deployed application data.

As of v0.8.0, support for [Restic](https://restic.net/) as the backup engine has been implemented, with the intention to deprecate [Duplicati](https://duplicati.readthedocs.io/) in favor of eventually supporting only Restic. For now, both backup engines will work, and for the sake of backwards compatibility, Duplicati is still the default engine.

Both engines take incremental snapshots of source files via a backup script, which is executed by Kubernetes CronJobs by default once per day. You may adjust this schedule using standard cron notation.

Applications typically store data in two locations:

1. flat files in some data volume
2. MySQL or PostgreSQL database

When using Restic, backup sets are stored in whatever volume is configured for the Restic server. By default this is a dynamically provisioned volume on the NFS-mounted storage system utilized by [the `nfsprovisioner` DecentCI infrastructure-level app](https://gitlab.com/decentci/kubernetes/-/blob/32bcaf43f4dbbc0f1c819d5aabe02b3dd0b5dc70/apps/infrastructure/values.yaml#L122-143). However, the backend storage is abstracted behind the Restic server, allowing for backup data to migrate as needed transparently to the backup operations. In fact, the Restic server itself could be deployed anywhere, providing additional flexibility as available resources evolve over time.

When using Duplicati, backup-specific storage volumes will be provisioned as NFS-mounted volumes given a server address and path; however, an existing volume of any type can be used by specifying an existing claim.

Database backups can be handled in one of two ways. If database connection information is provided in the `database` option, the database is dumped using either `mysqldump` or `pg_dump` depending on the database type. **If `source-pvc` is provided instead, the raw database files will be backed up directly; however, this should be used carefully because in general the database files may be in an inconsistent or broken state.**

Many applications have both a SQL database and a flat file store that *together* capture the application data state. **To reduce the risk of inconsistency between the database dump and the data files snapshot, schedule the CronJobs to execute at the same time.**

See the [`values.yaml` file](./values.yaml) for detailed documentation of the Backups Helm chart parameters.

## Backup configuration

There are two methods of configuring backups:

* (preferred)  Creating ConfigMap objects
* (deprecated) Adding annotations to Deployment and StatefulSet objects

### ConfigMaps

Creating a ConfigMap manifest (or multiple manifests) for a set of backups is the simplest and most flexible approach.

* The ConfigMap manifest must include the label `app.kubernetes.io/name: decentci-backups`.
* The ConfigMap `data` section must contain a `backups` key whose value is a YAML-formatted list of valid backup configs.
* The ConfigMap `data` section may optionally contain a `global` key whose value is a YAML-formatted set of backup configuration options that should be applied to each of the backups in the `backups` list.

 Consider the example below, where backups for a Nextcloud instance are configured. Two backups are configured (named `decentci-backups-nextcloud-files` and `decentci-backups-nextcloud-db`), both of which have Matrix alerts enabled. Only the database backup has a backup agent enabled because it overrides the `agent-enabled: false` option from the `global` section. The files backup has a custom cron `schedule` that overrides the chart default value. Restic has been selected instead of Duplicati for both backups, and the [Restic server](https://github.com/restic/rest-server) base URL has been provided.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: backups
  namespace: nextcloud
  labels:
    app.kubernetes.io/name: decentci-backups
    app.kubernetes.io/component: config
data:
  global: |
    enabled: true
    agent-enabled: false
    engine: "restic"
    restic:
      server:
        baseurl: "restic-server.example.com"
    alerts:
      matrix:
        existingSecret: backup-alerts
        urlKey: matrix-url
  backups: |
    - backup-name: decentci-backups-nextcloud-files
      source-pvc: data-nextcloud-db-0
      schedule: "21 7 * * *"
    - backup-name: decentci-backups-nextcloud-db
      agent-enabled: true
      database:
        type: mysql
        host: nextcloud-db
        auth:
          username: nextcloud
          database: nextcloud
          existingSecret: nextcloud-db
          passwordKey: mariadb-password
```

### Annotations

Adding annotations to Deployments and StatefulSets is another way to configure and enable backups for your applications. This method is deprecated, however, and will be removed in future releases.

To specify backup config parameters, create annotations for options shown in the next section, prepending the given parameter names with `decentci.backups/`. For example, the option `backup-name` would become the annotation `decentci.backups/backup-name`

### Backup configuration options

```yaml
## [required] Unique identifier for the backup associated with the 
## application data source (e.g. backup-myapp-files, backup-myapp-db)
backup-name: ''
## [required] Enable backups
enabled: 'false'
##
## Either `source-pvc` must be specified for flat file 
## backup or `database` must be specified for SQL 
## database backup (not both).
##
## Name of PersistentVolumeClaim containing the files to be backed up
source-pvc: ''
## Database configuration.
## Database types 'mysql' and 'postgresql' are supported.
## For annotations, the value of `database` is a JSON-formatted string:
##     decentci.backups/database: '{"type": "mysql", "host": "", ...}'
database:
  type: mysql
  host: ''
  auth:
    username: ''
    database: ''
    existingSecret: ''
    passwordKey: ''
##
## [optional] Backup CrobJob schedule (UTC timezone)
schedule: '13 7 * * *'
##
## [optional] Enable backups agent pod
agent-enabled: 'false'
##
## [optional] Select backup engine: "duplicati" or "restic".
engine: 'duplicati'
##
## BEGIN RESTIC-SPECIFIC CONFIG HERE
##
## If Restic is selected as the backup engine, configure the backup repo.
## See https://restic.readthedocs.io for Restic documentation.
restic:
  ## The information in this section will be used to populate the two environment variables
  ## needed for Restic: RESTIC_PASSWORD and RESTIC_REPOSITORY
  repo:
    ## Secret containing the Restic repo encryption password and/or restic config file contents
    existingSecret: "restic-auth"
    ## Name of key in the existingSecret whose value is the password. This value will populate env var `RESTIC_PASSWORD`.
    passwordKey: "restic-repo-password"
    ## Name of key in the existingSecret whose value is the contents of a complete Restic repo config.
    ## This data will be mounted as a file `/etc/restic.conf` that will be used to set all Restic 
    ## environment variables, overriding any that may already be set in the environment.
    configFileKey: ""
    ## Full URL value of RESTIC_REPOSITORY
    resticRepositoryBase: ""
    forgetArgs: "--keep-last=1 --keep-hourly=24 --keep-daily=7 --keep-weekly=4 --keep-monthly=6"
  server:
    ## Base URL of the target Restic server, e.g. restic-server.example.com/basepath. Omit trailing slashes.
    ## The information in this section will be combined with the `backup-name` value to construct the `RESTIC_REPOSITORY`
    ## env var:
    ##   RESTIC_REPOSITORY="rest:[protocol]://[username]:[password]@[baseurl]/[username]/[backup-name]/"
    baseurl: ""
    protocol: "https"
    ## Secret containing the Restic server access credentials
    existingSecret: "restic-auth"
    ## Name of key in the existingSecret whose value is the Restic server username
    usernameKey: "rest-server-username"
    ## Name of key in the existingSecret whose value is the Restic server password
    passwordKey: "rest-server-password"
##
## END RESTIC-SPECIFIC CONFIG HERE
##
## BEGIN DUPLICATI-SPECIFIC CONFIG HERE
##
## (For the sake of backwards compatibility, these Duplicati-specific settings have been
## left at the top level; however, now that Restic is supported, logically the options 
## in this section should be under `duplicati:`.)
##
## If Duplicati is selected, EITHER `backup-pvc` must be specified OR
## `nfs-server` and `nfs-path` must be specified.
##
## Target PersistentVolumeClaim where backup sets will be stored. Ignored when using Restic.
backup-pvc: ''
## Target NFS server and path where backup sets will be stored. Ignored when using Restic.
backup-nfs-server: 'taiga-nfs.ncsa.illinois.edu'
backup-nfs-path: '/taiga/ncsa/radiant/bbxx/mycluster/backups/myapp'
##
## [optional] Duplicati backup set retention policy. Ignored when using Restic.
## ref: https://duplicati.readthedocs.io/en/latest/06-advanced-options/#retention-policy
## The default policy is as follows: Retain all backups for the past seven days, 
## retain one backup per week for the past 4 weeks, and retain one backup per 
## month for the past year.
retention-policy: '7D:0s,4W:1W,12M:1M'
##
## END DUPLICATI-SPECIFIC CONFIG HERE
```

## Activating utility agent pods

For convenience, you can spawn utility agent pods that are preconfigured with an environment specific to a particular backup. To activate the pod, set the `agent-enabled` option to `true`. You can do this on the command line via `kubectl` like so:

```bash
kubectl edit -n myapp configmaps backups
```

where in this example `backups` is the name of the relevant ConfigMap and `myapp` is its namespace.

The new pod will have the same name as the backup as specified in the `backup-name` option, which you should attempt to make globally unique among all your backup names. Once the pod is online, open a terminal session using (`backups-myapp-files`)

```bash
kubectl exec -it -n myapp backups-myapp-files -- bash
```

where in this example `backups-myapp-files` is the name of the backup.

To destroy the agent pod, set the `agent-enabled` option to `false`. 

## Restic server

When backing up to a Restic server, the backups are namespaced by the Restic server username and the backup name as seen in the constructed URL example below. The `/username/backup-name` URL path maps directly to a file folder hierarchy on the Restic server data volume.

```
RESTIC_REPOSITORY="rest:https://username:password@restic-server.example.com/username/backup-name/"
```

You may choose to use a unique Restic server account for each application, or you can use a single account for all of them, relying on the backup name to demarcate the separate Restic backup repositories.

### User account provisioning

At the moment, account provisioning is not done automatically when a Restic server is deployed as part of the Backup system. In order for an app to use it as the target backup location, you must open a terminal in the Restic server pod and use the command line tool to create the user account to be used by the backups. In the example below, an account with username `agent` is created:

```bash
$ docker exec -n backups -it backups-restic-server-b6f4768bc-k2bwm create_user agent

New password: restic-server-password-for-agent
Re-type new password: restic-server-password-for-agent
Adding password for user agent
```

[See the documentation for more details](https://github.com/restic/rest-server#manage-users).

### Restic authentication Kubernetes Secret

After creating the user account, you must generate a Kubernetes Secret in the same namespace as the backup CronJob of the form:

```yaml
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: restic-auth
data:
  rest-server-password: "restic-server-password-for-agent"
  rest-server-username: "agent"
  restic-repo-password: "a-different-super-long-password"
```

## Other backup targets

When using any other remote backup target supported by Restic, you can override any and all environment variables used to configure the backup target by specifying `restic.repo.configFileKey`. This parameter specifies the key in the secret referenced by `restic.repo.existingSecret` whose value is a shell script that sets the Restic configuration env vars.

For example, to use an S3-compatible object store as the backup target, you could specify these values in your backup config "global" section:

```yaml
repo:
  existingSecret: "restic-auth"
  configFileKey: "restic-conf"
  resticRepositoryBase: "s3:https://object-store.example.com/my-bucket/backups/apps/myapp"
```

If you specify `restic.repo.resticRepositoryBase`, then the `RESTIC_REPOSITORY` env var will be constructed by path-appending the backup name to the given base URL. Using the backup name `backups-myapp-files` as in the example below, the result would be `s3:https://object-store.example.com/my-bucket/backups/apps/myapp/backups-myapp-files`.

You must generate a Kubernetes Secret in the same namespace as the backup CronJob of the form:

```yaml
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: restic-auth
data:
  restic-conf: |
    export AWS_ACCESS_KEY_ID="******"
    export AWS_SECRET_ACCESS_KEY="******"
```

The value of `restic-conf` will be mounted to the backup job/agent as `/etc/restic.conf` which will be sourced by the backup script, overriding any environment variables with the same names, prior to invoking restic.

## Perform manual backup

To launch a backup job manually:

```bash
kubectl create -n myapp job --from=cronjob/backups-myapp-files backups-myapp-files-manual
```

To delete the job when completed:

```bash
kubectl delete -n myapp job backups-myapp-files-manual
```

## Restore data from a backup

From a utility pod, restore the files from the backup set to a temporary path in the local filesystem:

```bash
# Restic
restic restore latest --target /tmp/restore
```

```bash
# Duplicati
duplicati-cli restore \
    /backup/ \
    "*" \
    --restore-path=/tmp/restore \
    --no-encryption=true
```

Two different approaches are taken depending on whether you are restoring a database or a flat file backup as described in the following subsections.

(When using Restic, it is possible to restore from backups without using the utility agent pods. While this is advanced and will not be described in detail here, you can restore the data locally on your workstation and then use `kubectl cp`  for flat files, and a combination of `kubectl port-forward` and `psql`/`mysql` for databases.)

### Restore flat files

The source data volume is mounted at `/data`, allowing you to copy the restored files into the source data volume using a standard utility like `rsync` as shown below.

```bash
rsync -n -va --delete /tmp/restore/ /data/
```

### Restore a database

Use the utility script `restore_db.sh` to restore to a SQL database by executing

```bash
./restore_db.sh /tmp/restore/myapp.sql
```

This will only work if the relevant database is online and ready to receive connections.

## Backup summary (Restic only)

When using Restic as the backup engine, you can use the utility script [`restic_backups_check.sh`](src/restic_backups_check.sh) to generate a convenient summary report of recent backup operations.

```bash
$ ./restic_backups_check.sh --help

Usage:  restic_backups_check.sh [OPTIONS]

Generate a Markdown-formatted summary of recent DecentCI backups using Restic.

  -u, --unlock               Attempt to remove stale restic repo locks
  -b, --backup-list string   Path to backup name list file

Export an environment variable RESTIC_REPOSITORY_BASEURL of the form below. It will be appended
with the backup name to form the complete RESTIC_REPOSITORY value required by Restic.

    export RESTIC_REPOSITORY_BASEURL=rest:https://username:password@restic.example.com/username

Restic also needs the repo password, so export RESTIC_PASSWORD as well:

    export RESTIC_PASSWORD=3c76874010dfff6feed4894de0ee134c084a07c8

Construct a file "backups.csv" with a list of backup names, one per line:

    backup-myapp-files
    backup-myapp-db
    ...

Given these files, an example command is:

    restic_backups_check.sh -b backups.csv | tee backup-summary.md

```

## Restic command line examples

Reference: https://restic.readthedocs.io/

One of the advantages of using Restic in conjunction with the Restic server is that you can access your backup sets trivially from any machine (assuming you enabled a publicly accessible ingress for the Restic server). After installing the Restic binary, populate two environment variables of the form

```bash
export RESTIC_REPOSITORY="rest:https://username:password@restic-server.example.com/username/backups-myapp-files/"
export RESTIC_PASSWORD="repo-password"
```

and then execute Restic commands, such as
```bash
# Show overall statistics about the backup repo
$ restic stats

repository 6ff6775a opened (version 2, compression level auto)
scanning...
Stats in restore-size mode:
     Snapshots processed:  2
        Total File Count:  106
              Total Size:  315.462 MiB
```

```bash
# List all the snapshots
$ restic snapshots
repository 6ff6775a opened (version 2, compression level auto)
ID        Time                 Host                           Tags        Paths
-------------------------------------------------------------------------------
cee1d4dc  2023-09-07 10:42:36  backups-myapp-files                        /data
b200bf8c  2023-09-07 15:40:50  backups-myapp-files                        /data
-------------------------------------------------------------------------------
2 snapshots
```

```bash
# List the files in the latest snapshot
$ restic ls latest

repository 6ff6775a opened (version 2, compression level auto)
snapshot b200bf8c of [/data] filtered by [] at 2023-09-07 20:40:50.064191231 +0000 UTC):
...
/data/archive/event/1/contribution/1/15-14-2023_CMF_report.pdf
/data/archive/event/1/contribution/29/10-10-MUSES_CI_diagram.pdf
...
```


```bash
# List the differences between the latest two snapshots
$ restic diff $(restic snapshots --json | jq -r '.[-2:][].id') 
repository 25fbd875 opened (version 2, compression level auto)
comparing snapshot c9321c9c to f8e62f0a:

M    /data/data/appdata_oc5cxg7e49wg/appstore/apps.json
+    /data/data/appdata_oc5cxg7e49wg/preview/0/0/a/c/c/0/5/246853/256-256-crop.png
+    /data/data/appdata_oc5cxg7e49wg/preview/1/4/3/a/2/7/5/246851/256-256-crop.png
+    /data/data/appdata_oc5cxg7e49wg/preview/8/f/2/f/f/a/c/244435/1024-1024.png
+    /data/data/appdata_oc5cxg7e49wg/preview/9/a/0/7/6/8/6/244436/1024-1024.png
+    /data/data/appdata_oc5cxg7e49wg/preview/c/3/a/a/b/a/a/246211/455-256.jpg
+    /data/data/appdata_oc5cxg7e49wg/preview/c/5/4/6/e/9/7/245222/1024-1024.png
+    /data/data/appdata_oc5cxg7e49wg/preview/e/f/1/8/9/f/1/246850/256-256-crop.png
M    /data/data/nextcloud.log

Files:           7 new,     0 removed,     2 changed
Dirs:            0 new,     0 removed
Others:          0 new,     0 removed
Data Blobs:      6 new,     2 removed
Tree Blobs:     61 new,    61 removed
  Added:   4.422 MiB
  Removed: 3.952 MiB
```

There are more useful commands including `restic mount` that you can learn about at https://restic.readthedocs.io.

## Duplicati command line examples

Reference: https://duplicati.readthedocs.io/en/latest/04-using-duplicati-from-the-command-line/

The Duplicati backup files will be mounted under `/backup`. If the utility agent is configured for flat file backup, the source data will be mounted under `/data`. In the case of a database backup, it is assumed that restoration will be done using the `mysql` or `psql` commands from a database dump file restored somewhere temporarily in the pod's local filesystem like `/tmp/myapp_db.sql` for example.

### List and search files in a backup

List the backup sets.

```
$ duplicati-cli find \
    /backup/ \
    --dbpath=/opt/duplicati/backups-myapp-files.sqlite \
    --no-encryption=true

Listing filesets:
0	: 01/24/2023 18:54:28 (11 files, 73.15 MB)
1	: 01/18/2023 01:13:14 (10 files, 54.60 MB)
```

List the files in the latest backup set.

```
$ duplicati-cli find \
    /backup/ \
    "*" \
    --dbpath=/opt/duplicati/backups-myapp-files.sqlite \
    --no-encryption=true

Listing contents 0 (01/24/2023 18:54:28):
...
/data/data/error.log (777 bytes)
/data/data/upload/logo3.png (19.37 KB)
...
```

List the files in the backup set `version=1`.

```
$ duplicati-cli find \
    /backup/ \
    "*" \
    --version=1 \
    --dbpath=/opt/duplicati/backups-myapp-files.sqlite \
    --no-encryption=true

Listing contents 1 (01/18/2023 01:13:14):
...
/data/data/error.log (777 bytes)
/data/data/upload/logo3.png (19.37 KB)
...
```

### Compare backup sets

Compare the backup set 1 to the latest backup set.

```
$ duplicati-cli compare \
    /backup/ \
    1 0 \
    --dbpath=/opt/duplicati/backups-myapp-files.sqlite \
    --no-encryption=true

Listing changes
  1: 01/18/2023 01:13:14
  0: 01/24/2023 18:54:28

Size of backup 1: 54.60 MB

  1 added entries:
  + /data/data/kuma.2023-01-18.db

  4 modified entries:
  ~ /data/data/
  ~ /data/data/kuma.db
  ~ /data/data/kuma.db-shm
  ~ /data/data/kuma.db-wal


  Added files:       1
  Modified folders:  1
  Modified files:    3
Size of backup 0: 73.15 MB

```

### Repair a backup

Occasionally you may encounter a situation where backups fail because of an inconsistency between the remote files and the local Duplicati database. For example, the CronJob log below shows that a remote file is missing:

```
Copying Duplicati database file from persistent storage...
Executing backup...
Backup started at 03/02/2023 16:56:11
Checking remote backup ...
  Listing remote folder ...
Missing file: duplicati-20230127T071313Z.dlist.zip
Found 1 files that are missing from the remote storage, please run repair
Fatal error => Found 1 files that are missing from the remote storage, please run repair

ErrorID: MissingRemoteFiles
Found 1 files that are missing from the remote storage, please run repair
```

If this occurs, enable the utility agent pod as described above, open a terminal to the pod, and run the Duplicati repair command:

```
root@backups-myapp-files:~/scripts# duplicati-cli repair /backup/ --no-encryption=true --dbpath=/opt/duplicati/backups-myapp-files.sqlite 

  Listing remote folder ...
  Uploading file (364.20 KB) ...
```

where `backups-myapp-files` is the name of the backup configured via the `backup-name` annotation. A subsequent backup should succeed.

## Upgrade notes

### v0.7.x to v0.8.0

* Support for using a new backup engine, Restic, has been implemented. Restic is now the preferred backup engine. Duplicati is deprecated and may eventually be dropped.
* The option to deploy a Restic backup server was added to the Backup system for a centralized, efficient storage backend for your applications using the Restic engine.

### v0.6.x to v0.7.0

* Support for backup configuration using ConfigMap objects has been introduced. Annotation-based configuration is deprecated, but no definite plans for removing support have been declared.
* There remains a known bug that causes multiple cycles of backup asset updates upon a single change to a Deployment or StatefulSet annotation. However, using ConfigMaps bypasses this issue.
* Improvements to the update process have been made, such that objects are patched instead of replaced when possible.

### v0.5.x to v0.6.0

* PostgreSQL in the backup agent was updated from v14 to v15

### v0.4.x to v0.5.0

* The backup system has been overhauled. Activating backups for an application no longer requires a Helm dependency. Backup configurations are instead specified by adding annotations to Deployments and StatefulSets in your applications. A new persistent service watches for changes to these object kinds and creates CronJobs, PV/PVC pairs, and Pods to implement the configured backups. 

### v0.3.x to v0.4.0

* The new backup engine Duplicati needs an empty directory for the initial backup. To retain existing backups created by `rsnapshot`, the backup target directory specified by the `volume.nfs.basePath` parameter should be changed to reference a new, empty directory. Otherwise, you will need to delete or move existing backup files from the target directory.
* The parameter `numSnapshots` is obsolete and can be removed.
* [Duplicati](https://duplicati.readthedocs.io/) has replaced rsnapshot as the backup engine. Duplicati is a much more sophisticated backup tool that will provide a much richer set of features and flexibility in the future. Currently only the [retention policy](https://duplicati.readthedocs.io/en/latest/06-advanced-options/#retention-policy) is configurable, but at some point we may support encrypted backups and remote target locations natively supported by Duplicati including S3 buckets.
