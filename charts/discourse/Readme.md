# Discourse

## Upgrade from Discourse 3.0.6 to 3.2.0

There is a bug in the Bitnami Helm chart that prevents a smooth update from v11 to v12 (Discourse version 3.0.6 to 3.2.0), even when upgrading through minor versions. There is [a discussion here](https://meta.discourse.org/t/discourse-v3-0-6-to-v3-1-1-upgrade-error-undefined-method-register-bookmarkable-for-bookmark-class/281264) and [a GitHub issue here](https://github.com/bitnami/charts/issues/19461).

The instructions below show how to navigate the upgrade. The "production site" is Helm chart release of the Discourse site being upgraded, and "migration site" refers to a temporary chart release deployed in parallel in namespace `discourse-dev`.

### Production site

1. Set production forum to read-only mode in Backups admin panel /admin/backups.
1. Make backup using Backups admin panel. Download backup archive locally.

### Migration site

1. Install fresh instance of Bitnami chart v12.6.2 (Discourse v3.2.0). Scale the Discourse server Deployment to zero.
    ```bash
    $ kubectl scale -n discourse-dev deployment discourse --replicas 0
    deployment.apps/discourse scaled
    ```
1. Create SQL file `db_init.sql`:

    ```bash
    cat > /tmp/db_init.sql << EOF
    DROP DATABASE bitnami_application;
    CREATE DATABASE bitnami_application;
    GRANT ALL PRIVILEGES ON DATABASE bitnami_application TO bn_discourse;
    CREATE EXTENSION hstore;
    CREATE EXTENSION pg_trgm;
    ALTER database bitnami_application owner to bn_discourse ;
    EOF
    ```
1. Copy to the database pod and execute:
    ```bash
    $ kubectl cp -n discourse-dev db_init.sql discourse-dev-postgresql-0:/tmp/

    $ kubectl exec -n discourse-dev -it discourse-dev-postgresql-0 -- bash -c 'PGPASSWORD=$POSTGRES_PASSWORD psql -U postgres < /tmp/db_init.sql'
    DROP DATABASE
    CREATE DATABASE
    GRANT
    ERROR:  extension "hstore" already exists
    ERROR:  extension "pg_trgm" already exists
    ALTER DATABASE
    ```
1. Extract backup archive, copy SQL dump file to database pod, and apply:

    ```bash
    $ ls
    discourse-2024-03-04-143102-v20230119094939.tar.gz
    $ tar xvf discourse-2024-03-04-143102-v20230119094939.tar.gz 
    $ gunzip dump.sql.gz 
    $ kubectl cp -n discourse-dev dump.sql discourse-dev-postgresql-0:/tmp/
    $ kubectl exec -n discourse-dev -it discourse-dev-postgresql-0 -- bash -c \
        'PGPASSWORD=$POSTGRES_PASSWORD psql -U bn_discourse --dbname bitnami_application < /tmp/dump.sql'
    ```
1. Launch Discourse pod. 
    ```bash
    $ kubectl scale -n discourse-dev deployment discourse --replicas 1
    deployment.apps/discourse scaled
    ```
1. Watch logs and drop tables that are preventing the upgrade:
    ```
    $ while true; do kubectl logs --prefix -f -n discourse-dev -c discourse -l app.kubernetes.io/name=discourse; sleep 5; done
    $ while true; do kubectl logs --prefix -f -n discourse-dev -l app.kubernetes.io/name=postgresql; sleep 5; done
    ```
1. Watch while db migrations fail and manually delete the relevant tables until migrations succeed:
    ```
    $ kubectl exec -n discourse-dev -it discourse-dev-postgresql-0 -- bash -c \
        'PGPASSWORD=$POSTGRES_PASSWORD psql -U bn_discourse --dbname bitnami_application -c "\
        DROP TABLE sidebar_sections; \
        DROP TABLE sidebar_urls; \
        DROP TABLE chat_threads; \
        DROP TABLE form_templates; \
        DROP TABLE category_settings; \
        DROP TABLE category_form_templates; \
        DROP TABLE theme_svg_sprites; \
        DROP TABLE user_chat_thread_memberships; \
        DROP TABLE summary_sections; \
        "'
    ```
1. Restore uploads folder backup:
    ```
    $ PODNAME=$(kubectl get pod -n discourse-dev -l app.kubernetes.io/name=discourse --output=jsonpath={.items..metadata.name} | cut -f1 -d' ')
    $ kubectl cp -n discourse-dev -c discourse uploads $PODNAME:/bitnami/discourse/public/
    $ kubectl exec -n discourse-dev -c discourse $PODNAME -- bash -c 'chown -R 999:0 /bitnami/discourse/public/uploads/'
    ```

1. For some reason, the plugins did not automatically download and install. Do this manually and verify plugin functionality.
    ```bash
    root@discourse-54c6d6fb7c-z9285:/bitnami/discourse/plugins$ git clone https://github.com/discourse/discourse-oauth2-basic
    root@discourse-54c6d6fb7c-z9285:/bitnami/discourse/plugins$ git clone https://github.com/discourse/discourse-math
    root@discourse-54c6d6fb7c-z9285:/bitnami/discourse/plugins$ chown -R 999:0 discourse-oauth2-basic discourse-math
    ```
1. Create a backup of the newly restored site using Backups admin panel.

### Production site

1. Delete the original deployment and purge PV/PVCs.
1. Install fresh instance of Bitnami chart v12.6.2 (Discourse v3.2.0).
1. Use `https://$BASE_URL/u/admin-login` to login by email link.
1. Upload and restore backup archive using Backups admin panel.
