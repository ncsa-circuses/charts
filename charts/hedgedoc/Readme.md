# HedgeDoc Helm chart

This chart deploys a high-availability instance of [HedgeDoc](https://docs.hedgedoc.org).

## Secrets

Supply a Secret for the database with the following form:

```yaml
kind: Secret
apiVersion: v1
metadata:
  name: hedgedoc-db
type: Opaque
data:
  password: ""
  postgres-password: ""
  postgresql-password: ""
  postgresql-postgres-password: ""
  replication-password: ""
  repmgr-password: ""
  admin-password: ""
```

If OAuth is enabled, supply a Secret with the following form:

```yaml
kind: Secret
apiVersion: v1
metadata:
  name: hedgedoc-oauth
type: Opaque
data:
  clientId: ""
  clientSecret: ""
```
